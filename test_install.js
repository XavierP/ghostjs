/*jshint strict:false*/
/*global CasperError, console, phantom, require*/

/**
 * @TODO: CHANGE YOUR GHOSTJS library path here
 */
var libsPath = '/home/wizome/dev/libs/ghostJS/';

if (phantom.casperEngine !== "slimerjs") {

	var Ghost = require('casper').Casper,
	    system = require('system'),
	    Casper = require(libsPath + "ghost").Casper,
	    randomizer = require(libsPath + "randomUtils"),
	    Mouse = require("mouse"),
	    utils = require('utils'),
	    x = require('casper').selectXPath,
	    fs = require('fs'),
	    f  = require('utils').format;
}
else {
    require.paths.push(libsPath);
    Casper = require('ghost').Casper;
}



/**
 * Create new Casper instance
 * Initialize with default parameters
 */
var casper = new Casper({
    
    //logLevel: 'debug',
    logLevel: 'info',

    waitTimeout: 25000,             // Default wait timeout
    pageSettings: {
        webSecurityEnabled: false,
        loadImages:  true,        // The WebPage instance used by Casper will load images
        loadPlugins: false         // nor plugins
    },

    stepTimeout: 25000,           // Default step timeout
    verbose: true,
});


// Set current site configuration
casper.configJSON = {
    urls : {},
    xpaths: {
      ip:       "//span[@id='ip']",
      port:     "//p[starts-with(strong, 'Port Utilisé')]/text()[1]",
      country:  ["//li[starts-with(., 'Pays de Connexion')]/text()", {extract: /Pays de Connexion :(.+)/}]
    },
    user: {
      id: 0,
    },
    tmpPath: {
      screens: "/tmp/casperjs/tests/screens/", 
      cookies: "/tmp/casperjs/tests/cookies/", 
      htmls:  "/tmp/casperjs/tests/htmls/",
      jsons:  "/tmp/casperjs/tests/jsons/",
    },

    siteExclusions: [
      "google"
    ],
};


// Disable screenshots
casper.disableScreenshots = false;

// Set current files names prefix
//casper.filesPrefix = "carrefour";

// Set the init page min. waiting time
casper.initWaitTime = 1;

// Config the custom wait multiplier
casper.customWaitMultiplier = 1;

// Set the viewport sizes
casper.viewPortSize.width = 800;
casper.viewPortSize.height = 1200;

// Set custom search results files paths
//casper.customSearchResultsHTMLs = "/tmp/casperjs/tests/htmls/";
//casper.customSearchResultsJSONs = "/tmp/casperjs/tests/jsons/";

// Deal with input parameters specified as command line arguments
if(casper.cli.has("filesPrefix"))
	casper.filesPrefix = casper.cli.get("filesPrefix");


var baseurl = casper.cli.has("url")? casper.cli.get("url") : "http://www.mon-ip.com/info-adresse-ip.php";


// Init vars
var jsonresults = null;

// Init browser and load page
casper.init(baseurl, "home-site");

// Wait a couple of seconds
casper.thenWait(2,3, 1000);


// Parse data
casper.then(function() {
    jsonresults = this.parseComplexData(this.configJSON.xpaths);
});


casper.then(function() {
  
    this.echo("RES: " + JSON.stringify(jsonresults, null, 3));
    this.saveJSON(jsonresults, this.configJSON.tmpPath.jsons + "test.json");
});

casper.execute();