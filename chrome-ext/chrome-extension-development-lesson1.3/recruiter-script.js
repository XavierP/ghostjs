const mapping = {
    idCv: "//input[@id='blocDetailIdCv']/@value",
    cvs: {
        joint: "//div[@id='PopinCV']//a[contains(@onclick, 'cvJoint')]",
        pe: "//div[@id='PopinCV']//a[contains(@onclick, 'cvPoleEmploi')]",
    },
    currentPage: [
        "//span[@class='pagination-number']/strong",
        {extract: "(\\d+)"}
    ],
    nextPageButton: "//div[@class='modal-cv-pager']/div/button[contains(@class, 'next')]",
};


function doPromiseIfExists(xpath, treatmentCb, validationMsg) {

    return __utils__.selectorExists(xpath).then(count => {

        return new Promise( (resolve, reject) => {
            setTimeout( () => {
                treatmentCb().then(()=>{
                    console.log(validationMsg || "Button less has been found");
                    resolve();
                }).catch(errmsg=>{
                    reject(errmsg);
                });
                
            }, Math.floor((Math.random()+0.25) * 500)*2);
        });
        
    }, count => {
        console.log("HTML node could not be found: " + xpath);
        throw("NODE_NOT_FOUND");
    });
}


function timerPromise(mult=1) {
    console.log(" [timerPromise] Entering time Promise...");
    return new Promise( (resolve, reject) => {
        setTimeout( () => {
            resolve();
        }, Math.floor((Math.random()+0.25) * 500)*2*mult);
    });
}


function browseAllPages() {
    let pagenum = null;
    return timerPromise(3)
    .then(() => {
        let datares = parseComplexData(mapping);
        return datares;
    })
    .then( datares => {
        pagenum = datares.currentPage;
        console.log(" Data parsed ("+pagenum+") : " + JSON.stringify(datares, null, 2));

        console.log("[ In-Promise] About to download current profile's CV...");

        return __utils__.selectorExists(mapping.cvs.joint)
        .then(count => {
            __utils__.getElementByXPath(mapping.cvs.joint).click();
        })
        .catch(err=> {
            __utils__.getElementByXPath(mapping.cvs.pe).click();
        });

    })
    .then( () => timerPromise(2))
    .then(() => {
        return doPromiseIfExists(mapping.nextPageButton, () => {

            return timerPromise()
            .then(() => {
                console.log("[ In-Promise] About to click on button page n°"+(pagenum+1)+"...");
                __utils__.getElementByXPath(mapping.nextPageButton).click();
                return _Utils_.waitForSelector(`${mapping.currentPage[0]}[.='${pagenum+1}' or .='${pagenum+2}']`);
            })

        }, "Next page button has been found, and clicked ...")
        .then(() => {

        })
        //.then(() => browseAllPages(pagenum+1))
        .then(() => browseAllPages())
        .catch(errmsg => {
            if (errmsg==="NODE_NOT_FOUND") {
                console.log(" Next page button could not be found ! ");
                throw("NO_NEXT_PAGE");
            }
            else 
                throw(errmsg);
        });
    });
}

chrome.extension.onMessage.addListener(function (message, sender, sendResponse) {
    
    switch (message.type) {

        case "init-recruiter-cvs":

            doPromiseIfExists("(//h2/button)[1]", () => {

                //sessionStorage.pagenum = 1;

                chrome.storage.local.clear(function() {
                    var error = chrome.runtime.lastError;
                    if (error) {
                        console.error(error);
                    }
                    else console.log("Local storage has been successfully cleared !");
                });
                

                return timerPromise()
                .then(() => {
                    console.log("[ In-Promise] About to click on first result link...");
                    __utils__.getElementByXPath("(//h2/button)[1]").click();
                    return _Utils_.waitForSelector("//span[@class='pagination-number']/strong[.='1']");
                })
                /*
                .then( () => timerPromise(2))
                .then(() => {
                    var datares = parseComplexData(mapping);
                    return datares;
                })
                .then( datares => {

                    console.log("[ In-Promise] About to download current profile's CV...");
                    if(datares.cvs.hasOwnProperty("joint"))
                        __utils__.getElementByXPath(mapping.cvs.joint).click();
                    else
                        __utils__.getElementByXPath(mapping.cvs.pe).click();
                })
                */
                .then(() => browseAllPages())
                .catch( err => {

                    if(err === "NO_NEXT_PAGE")
                        alert("I'm done ! : )");
                    else throw err;
                });

            }, "First result link has been found, and clicked ...")
            .catch(errmsg => {
                if (errmsg==="NODE_NOT_FOUND") {
                    console.log(" First result link could not be found ! ");
                    alert("First result link could not be found !");
                }
                throw(errmsg);
            });

        break;
    }

    //return true;
});