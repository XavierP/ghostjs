/*
chrome.browserAction.onClicked.addListener(function() {
    // The event page will unload after handling this event (assuming nothing
    // else is keeping it awake). The content script will become the main way to
    // interact with us.
    chrome.tabs.create({url: "https://candidat.pole-emploi.fr/offres/recherche?dureeHebdo=1&emission=3&offresPartenaires=false&rayon=10&tri=0&typeContrat=CDI"}, function(tab) {

    
      chrome.tabs.executeScript(tab.id, {file: "script.js"}, function() {
        // Note: we also sent a message above, upon loading the event page,
        // but the content script will not be loaded at that point, so we send
        // another here.

        initStep();
      });
      
    });
    
});
*/

/*
chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
    if (changeInfo.status == 'complete') {
        // Execute some script when the page is fully (DOM) ready
        //chrome.tabs.executeScript(null, {code:"init();"});
        urlUpdatedNotification(changeInfo, tab.url);
    }
});
*/

let urls = [];
let totalPagesCount = null;

const notRunWithinTheLastSecond = (dt) => {
    const diff = dt - chrome._LAST_RUN;
    if (diff < 1000){
      return false;
    } else {
      return true;
    }
  };

chrome.webNavigation.onHistoryStateUpdated.addListener(function(details) {

    if(details.frameId === 0) {

        //@see fix here: https://stackoverflow.com/questions/36808309/chrome-extension-page-update-twice-then-removed-on-youtube
        if(typeof chrome._LAST_RUN === 'undefined' || notRunWithinTheLastSecond(details.timeStamp)){

            chrome._LAST_RUN = details.timeStamp;

            // Fires only when details.url === currentTab.url
            chrome.tabs.get(details.tabId, function(tab) {

                if(tab.url === details.url && urls.indexOf(tab.url)===-1) {
                    //console.log("onHistoryStateUpdated");
                    urls.push(tab.url);
                    urlUpdatedNotification(details);
                }
                else if(urls.indexOf(tab.url)!==-1 && tab.url === details.url){
                    console.log(` [onHistoryStateUpdated] Url has already been treated nefore: ${details.url}`);
                    urlUpdatedNotification(details);
                }
            });

        }
    }
});

// When user launch the process init via the button in extension's popup
chrome.extension.onMessage.addListener(function(request, sender, sendResponse) {

    switch(request.type) {

        case "download-cvs":

            chrome.tabs.query({url: "https://entreprise.pole-emploi.fr/recherche-cv/*"} , (tabs) => {
        
                if(tabs[0] !== undefined) {
        
                    let tab = tabs[0];
        
                    if(tab !== undefined && tab.hasOwnProperty("id") && tab.id>=0) {
        
                        chrome.tabs.executeScript(tab.id, {file: "utils.js"}, () => {
                            chrome.tabs.executeScript(tab.id, {file: "recruiter-script.js"}, () => {
                                // Note: we also sent a message above, upon loading the event page,
                                // but the content script will not be loaded at that point, so we send
                                // another here.
            
                                initRecruiterDownloadCVs();
                            });
                        });
                    }
                    else
                        console.log("Tab.getSelected returned less than 0 value ! ");
                    //chrome.tabs.sendMessage(tab.id, {type: "test", changeInfo: changeInfo});
                }
            });

        break;

        case "init-step":

            let contractType = (request.cdi && request.cdd) ? 'CDI,CDD' : (request.cdd? 'CDD' : 'CDI');

            chrome.tabs.create({
                
                url: `https://candidat.pole-emploi.fr/offres/recherche?dureeHebdo=1&emission=${request.duration}&lieux=${request.location}D&offresPartenaires=false&rayon=10&tri=0&typeContrat=${contractType}`}, tab => {

                chrome.tabs.executeScript(tab.id, {file: "utils.js"}, () => {
                    chrome.tabs.executeScript(tab.id, {file: "script.js"}, () => {
                        // Note: we also sent a message above, upon loading the event page,
                        // but the content script will not be loaded at that point, so we send
                        // another here.

                        initStep(request.location);
                    });
                });
            });
            break;

    }
    return true;
});

var initRecruiterDownloadCVs = () => {

    chrome.tabs.getSelected(null, (tab) => {

        chrome.tabs.sendMessage(tab.id, {type: "init-recruiter-cvs"});

        /*
        chrome.tabs.sendMessage(tab.id, {type: "init-recruiter-cvs"}, response => {

            if(response !== undefined && response.hasOwnProperty("idCv") && response.hasOwnProperty("typeCv")) {

                (function downloadCV(idCv, typeCv="cvPoleEmploi") {

                    chrome.downloads.download({
                        url: "https://entreprise.pole-emploi.fr/recherche-cv/recherchecv:eventtelechargercv",
                        method: "POST",
                        body: "idCv="+idCv+"&typeCv="+typeCv,
                        filename: idCv + "_" + typeCv + ".pdf"
                    });
                })(response.idCv, response.typeCv);
            }
        });
        */

    });
};

var initStep = function(location) {
    chrome.tabs.getSelected(null, (tab) => {
        chrome.tabs.sendMessage(tab.id, {type: "init-step", location: location});
    });
};


var urlUpdatedNotification = function(changeInfo) {

    chrome.tabs.query({url: "https://*.pole-emploi.fr/offres/recherche/detail/*"} , (tabs) => {
    //chrome.tabs.getSelected(null, (tab) => {

        if(tabs[0] !== undefined) {

            let tab = tabs[0];

            if(tab !== undefined && tab.hasOwnProperty("id") && tab.id>=0) {

                chrome.tabs.sendMessage(tab.id, {type: "url-changed", changeInfo: changeInfo}, response => {

                    if(response !== undefined) {

                        console.log("Background got response after sending notif message: " + JSON.stringify(response, null, 2));

                        totalPagesCount = response.totalcount;

                        if(response.hasOwnProperty("nextref") && response.nextref !== null && response.nextref !== undefined && response.nextref!=="") {
                            chrome.tabs.sendMessage(tab.id, {type: "click-next-page", nextref: response.nextref});
                        }
                        else if(response.hasOwnProperty("prevref") && response.prevref !== null && response.prevref !== undefined && response.prevref!=="") {
                            chrome.tabs.sendMessage(tab.id, {type: "click-next-page", prevref: response.prevref});
                        }
                        else if(response.hasOwnProperty("alldone") && response.alldone) {
                            chrome.tabs.sendMessage(tab.id, {type: "all-done"});
                        }
                    }
                    else console.log("Got empty response : undefined !!");
                });
            }
            else
                console.log("Tab.getSelected returned less than 0 value ! ");
            //chrome.tabs.sendMessage(tab.id, {type: "test", changeInfo: changeInfo});
        }
    });
};