// Popup
window.onload = function() {

    document.getElementById("recruiter").onclick = () => {

        chrome.extension.sendMessage({
            type: "download-cvs"
        });
    };

    document.getElementById("button").onclick = () => {

        let location = document.getElementById("location").value;
        let durationSelect = document.getElementById("duration");
        let duration = durationSelect.options[durationSelect.selectedIndex].value;
        let input_cdd = document.getElementById("cdd").checked;
        let input_cdi = document.getElementById("cdd").checked;

        if(location!==null && location!=="" && /^\d\d\d?$/i.test(location) && duration!==null && duration!=="" && /^(?:1|3|7|14|31)?$/i.test(duration)) {
            chrome.extension.sendMessage({
                type: "init-step",
                location: location,
                duration: duration,
                cdd: input_cdd,
                cdi: input_cdi
            });
        }
        else
            alert("Location must be a department two digits number !");
    };
}