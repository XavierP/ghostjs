// https://stackoverflow.com/questions/40328932/javascript-es6-promise-for-loop
// https://stackoverflow.com/questions/33464813/javascript-perform-a-chain-of-promises-synchronously
console.log(" [injectFile] File Utils.js has been loaded !");

/**
* Copy (deep) an object, not by reference
* @param {Object} o - The object to copy
* @return A copy of the input object
*/
var objcopy = objcopy || function _objcopy(o) {

    var out, v, key;
    out = Array.isArray(o) ? [] : {};

    for (key in o) {
    
        v = o[key];
    
        if(v instanceof RegExp || (v === Object(v) && v.constructor.name==="RegExp")) {
            out[key] = v;
        }
        else {
            out[key] = (typeof v === "object") ? objcopy(v) : v;
        }
    }
    return out;
};

/**
 * Parse complex data structure, based on a JSON mapping config
 * @param {Object} mapping - Object representing the label -> xpath mapping
 * @param {string} wrapperXPath - The XPath expression of the current object's wrapper
 * @return {Object} JSON object containing extracted data, based on the provided mapping
 */
var parseComplexData = parseComplexData || function _parseComplexData(mapping, wrapperXPath) {

    wrapperXPath = (wrapperXPath===undefined || wrapperXPath===null)? "" : wrapperXPath;
        
    let datares = {};

    // IF mapping is an object
    if(mapping === Object(mapping) && !Array.isArray(mapping)) {

        let wrapperitemscount = 0;

        if (mapping.hasOwnProperty("_wrapper_")) {

            // Append current wrapper XPath to the context existing one, if any
            wrapperXPath+= mapping._wrapper_;

            // Copy the mapping object in order to gelete the _wrapper_ key, without affecting the mapping oject
            let _mapping = objcopy(mapping);
            
            delete _mapping._wrapper_;

            // Count the number of items wrapped
            // If current wrapper XPath contains an occurence of '[]', remove it for the items count step
            wrapperitemscount = __utils__.count(wrapperXPath.replace("[]", ""));

            console.log(" [PARSECOMPLEXDATA] wrapperitemscount : " + wrapperitemscount + " (for wrapper xpath "+wrapperXPath.replace("[]", "")+")");

            if (wrapperitemscount) {

                datares = [];
                //var indexes = [];

                // Fill in list of indexes to use
                for (let index = 1; index <= wrapperitemscount; index++) {
                    //indexes[i] = i;

                //for(var index in indexes) {

                    // If current wrapper XPath contains an occurence of '[]', replace it with current index: [index]
                    if(/\[\]/i.test(wrapperXPath)) {
                        let _wrapperXPath = wrapperXPath.replace("[]", "["+index+"]");
                        //console.log("Wrapper XPath is now (1): " + _wrapperXPath);
                        let parsingres = parseComplexData(_mapping, wrapperXPath.replace("[]", "["+index+"]"));
                        if(parsingres!==null && parsingres!==undefined)
                            datares.push(parsingres);
                    }
                    else {
                        // Else simply append the index predicat condition to the end of the wrapper XPath string
                        //datares.push(parseComplexData(_mapping, wrapperXPath + "["+index+"]"));
                        let _wrapperXPath =  "("+wrapperXPath + ")["+index+"]";
                        //console.log("Wrapper XPath is now (2): " + _wrapperXPath);
                        let parsingres = parseComplexData(_mapping, "("+wrapperXPath + ")["+index+"]")
                        if(parsingres!==null && parsingres!==undefined)
                            datares.push(parsingres);
                    }
                }
            }
        }
        else {

            let rawvalues = {};
            //console.log(" [GHOSTJS] Current context's wrapper: " + wrapperXPath);

            for(let label in mapping) {

                //console.log(" [GHOSTJS] Dealing with label= "+label+", mapping[label] = " + JSON.stringify(mapping[label]));
                //console.log(` [GHOSTJS] Dealing with label: '${label}'`);

                if (!mapping.hasOwnProperty(label) || label==="_wrapper_") {
                    continue;
                }

                // IF dealing with subobject
                if(mapping[label] === Object(mapping[label]) && !Array.isArray(mapping[label])) {
            
                    let subobj = parseComplexData(mapping[label], wrapperXPath);

                    // IF sub object/array is not empty
                    //if(JSON.stringify(subobj) !== JSON.stringify({})) {
                    if(!(Object.keys(subobj).length == 0 && (subobj.constructor === Object || subobj.constructor === Array) )) {
                        rawvalues[label] = datares[label] = subobj;
                    }

                    // If dealing with object of type "table" 
                    // exemple: table : {_wrapper_:"//div[@id='wrapper']", label: "/span[1]", data: "/span[2]"}
                    if((label==="table" || label==="_table_") && Array.isArray(datares[label]) && datares[label].length)  {
                        
                        if(datares[label][0].hasOwnProperty("label") && datares[label][0].hasOwnProperty("data")) {

                            var _tmpdata = {};
                            Array.prototype.map.call(datares[label], (e, eindex) => {
                                _tmpdata[e.label] = e.data;
                            });

                            if(label==="_table_") {
                                //datares = _tmpdata;
                                for(var _label in _tmpdata) {
                                    datares[_label] = _tmpdata[_label];
                                }
                    
                                delete datares._table_;
                            }
                            else {
                                datares[label] = _tmpdata;
                            }
                        }
                    }
            
                }
                // IF dealing with pair: Label=>XPath
                else {

                    let callbacksPipe = {};
                    let currentXPath = mapping[label];
                    let tmpres = null;

                    // IF XPath is not a string but an array containing string + callbacks
                    if(Array.isArray(currentXPath)) {
                        callbacksPipe = currentXPath[1];
                        //console.log("Callbacks pipe: " + JSON.stringify(callbacksPipe));
                        //if(callbacksPipe.extract === {}) console.log("Extract object is empty");
                        currentXPath = currentXPath[0];
                    }

                    // IF label to copy starts or ends with ":", and do not starts with "/"
                    if (/^[^\/]/i.test(currentXPath) && (/^:/i.test(currentXPath) || /:$/i.test(currentXPath))) {

                        var copylabel = currentXPath.replace(":", "");

                        if(/^:/i.test(currentXPath)) { // IF label to copy starts with ":"

                            if(rawvalues.hasOwnProperty(copylabel)) {

                                tmpres = Array.isArray(rawvalues[copylabel])? rawvalues[copylabel].slice() : rawvalues[copylabel];
                            }
                            else {
                                console.log("Could not find property '"+copylabel+"' within the 'rawvalues' object");
                            }
                        }
                        else if(/:$/i.test(currentXPath)) { // IF label to copy ends with ":"

                            if(datares.hasOwnProperty(copylabel)) {

                                tmpres = Array.isArray(datares[copylabel])? datares[copylabel].slice() : datares[copylabel];
                            }
                            else
                                console.log("Could not find property '"+copylabel+"' within the 'datares' object");
                        }
                    }
                    else {

                        if (wrapperXPath!=="" && /\|\//i.test(currentXPath)) {
                            var xpathpartsres = currentXPath.match(/(\/[^|]+)\|(\/[^|]+)/i);
                            currentXPath = "(" + wrapperXPath + xpathpartsres[1] + "|" + wrapperXPath + xpathpartsres[2] + ")[1]";
                        }
                        else {
                            currentXPath = wrapperXPath + currentXPath;
                        }

                        tmpres = parseComplexData(currentXPath);
                    }

                    //console.log(" Got data: '" + tmpres + "', for label: '" + label + "'");

                    // Get a new fresh copy by cloning the array and getting a reference to this newly created array
                    if(tmpres!==undefined && tmpres!==null) {
                        rawvalues[label] = tmpres.slice();
                    }

                    //var itemres = tmpres;
                    let itemres = (tmpres!==undefined)? (Array.isArray(tmpres) ? tmpres : [tmpres]) : [];


                    // Deals with the callbacks pipe, applying callbacks one by one, sequentially, in order they appear
                    for(let callback in callbacksPipe) {

                        let callbackParams = callbacksPipe[callback];
                        let callbackParam = null;
                        let tmpvalues = [];

                        //console.log("Dealing with callback: '" + callback + "', for label: '" + label + "', with params: " + callbackParams);
                        //console.log("callbackParams: "+ JSON.stringify(callbackParams));

                        if(callbackParams instanceof RegExp || (callbackParams === Object(callbackParams) && callbackParams.constructor.name==="RegExp") || typeof callbackParams === 'string' || callbackParams instanceof String) {
                            callbackParam = callbackParams;
                            //console.log(" [GOOD] callbackParams detected as REGEX !");
                        }
                        else if (callbackParams === Object(callbackParams)) {
                            //console.log(" [object] callbackParams detected as OBJECT ! Properties: " + JSON.stringify(Object.getOwnPropertyNames(Object(callbackParams))));
                        }
                        else {
                            //console.log(" [warning] callbackParams type could not be detected !");
                        }

                        //console.log("Callback param: " + callbackParam.toString()); //console.log("Callback params: " + JSON.stringify(callbackParams));


                        // Deal with callbacks creating array from single string
                        switch(callback) {

                            case "join":
                            case "merge":

                                if(Array.isArray(itemres)) {
                                    let joinseparator = (typeof callbackParam === 'string' || callbackParam instanceof String) ? callbackParam : " ";
                                    itemres[0] = itemres.join(joinseparator);
                                    itemres = itemres.slice(0, 1);
                                }
                            break;

                            case "count":
                                if(Array.isArray(itemres)) {
                                    itemres[0] = itemres.length;
                                    itemres = itemres.slice(0, 1);
                                }
                            break;

                            case "split":
                            case "explode":

                                if(Array.isArray(itemres)  && itemres.length) {

                                    let splitseparator = (typeof callbackParam === 'string' || callbackParam instanceof String) ? 
                                        callbackParam : (Array.isArray(callbackParams)? new RegExp(callbackParams.join("|")) : " ");

                                    itemres = itemres[0].split(splitseparator);
                                    itemres = itemres.map(s => s.trim());
                                }
                            break;

                            case "unique":

                                if(Array.isArray(itemres)) {
                                    itemres = itemres.reduce((x, y) => x.includes(y) ? x : [...x, y], []);
                                }

                                if(itemres === undefined)
                                    itemres = [];
                                
                            break;

                            
                            default:
                                break;
                        }
                        

                        // FOR EACH items found (matching the XPath criteria)
                        for (let i = 0; i < itemres.length; i++) {

                            //console.log("Dealing with callback: '" + callback + "', for label: '" + label + "', with value: " + itemres[i]);

                            switch(callback) {

                                case "atob":
                                    itemres[i] = atob(itemres[i]);
                                    break;

                                case "btoa":
                                    itemres[i] = btoa(itemres[i]);
                                    break;

                                case "decodeURI":
                                    try {
                                        itemres[i] = decodeURIComponent(itemres[i]);
                                    }
                                    catch(err) {
                                        console.log(` [parseComplexData.decodeURI] Could not decode: ${itemres[i]}`);
                                    }
                                    break;

                                case "remove":

                                    if(typeof callbackParam === 'string' || callbackParam instanceof String) {
                                        callbackParam = new RegExp(callbackParam, 'gi');
                                    }

                                    if (callbackParam !== null && (callbackParam instanceof RegExp || (callbackParam === Object(callbackParam) && callbackParam.constructor.name==="RegExp"))) {
                                        itemres[i] = itemres[i].replace(callbackParam, "");
                                    }
                                    break;

                                case "replace":

                                    let callbackAllParams = Array.isArray(callbackParams)? callbackParams : [callbackParams];

                                    for (let callbackParams of callbackAllParams) {

                                        if((callbackParams.hasOwnProperty("search") || callbackParams.hasOwnProperty("regex")) && callbackParams.hasOwnProperty("replace")) {

                                            if(!callbackParams.hasOwnProperty("search")) {
                                                callbackParams.search = [];
                                            }

                                            /*
                                            if(callbackParams.hasOwnProperty("regex")) {
                                                if(typeof callbackParams.regex === 'string' || callbackParams.regex instanceof String) {
                                                    callbackParams.search = new RegExp(callbackParams.regex, 'gi');
                                                }
                                                else callbackParams.search = callbackParams.regex;
                                            }
                                            */

                                            let searchItems = Array.isArray(callbackParams.search) ? callbackParams.search : [callbackParams.search];
                                            let replaceItems = Array.isArray(callbackParams.replace)? callbackParams.replace: [callbackParams.replace];

                                            if(callbackParams.hasOwnProperty("regex")) {

                                                let regexItems = Array.isArray(callbackParams.regex) ? callbackParams.regex : [callbackParams.regex];

                                                regexItems = regexItems.map((regexItem) => {
                                                    if(typeof regexItem === 'string' || regexItem instanceof String) {
                                                        return new RegExp(regexItem, 'gi');
                                                    }
                                                    else {
                                                        return regexItem;
                                                    }
                                                });

                                                searchItems.push(...regexItems);
                                            }

                                            
                                            for(let searchItem of searchItems) {
                                                itemres[i] = itemres[i].replace(searchItem, callbackParams.replace);
                                            }
                                            

                                            /*
                                            itemres[i] = searchItems.reduce((acc, searchItem, index) => {
                                                return acc.replace(searchItem, replaceItems.length===1? replaceItems[0] : replaceItems[index]);
                                            }, itemres[i]);*/

                                            /*
                                            if(Array.isArray(callbackParams.search)) {
                                                for(let searchParam of callbackParams.search) {
                                                    itemres[i] = itemres[i].replace(searchParam, callbackParams.replace);
                                                }
                                            }
                                            else {
                                                itemres[i] = itemres[i].replace(callbackParams.search, callbackParams.replace);
                                            }
                                            */
                                        }
                                    }
                                    break;

                                case "extract":

                                    itemres[i] = __utils__.extract(itemres[i], callbackParam);
                                    break;

                                case "extractall":

                                    if(typeof callbackParam === 'string' || callbackParam instanceof String) {
                                        callbackParam = new RegExp(callbackParam, 'gi');
                                    }

                                    if (callbackParam !== null && (callbackParam instanceof RegExp || (callbackParam === Object(callbackParam) && callbackParam.constructor.name==="RegExp"))) {

                                        //console.log("Extracting data from: " + itemres[i]);

                                        //if(itemres[i]!==null)
                                        rgxres = itemres[i].match(callbackParam);


                                        if(rgxres !== null && rgxres!==undefined) {

                                            itemres[i] = rgxres[0];

                                            for(let j=1; j<rgxres.length; j++) {
                                                if(rgxres[j]!==null)
                                                    tmpvalues.push(rgxres[j]);
                                            }
                                        //console.log("Regex res: " + JSON.stringify(rgxres));
                                        }
                                        else itemres[i] = "";
                                    }
                                    break;

                                case "formatInt":
                                
                                    if(/\d\s?k$/i.test(itemres[i])) {
                                        itemres[i] = 1000 * parseFloat(itemres[i].replace(",", "."));
                                    }
                                    else {
                                        itemres[i] = itemres[i].replace(/,|\.|\s/i, "");
                                    }

                                    itemres[i] = parseInt(itemres[i]);
                                break;

                                case 'formatToDateStringEN':

                                    let sep = callbackParams.hasOwnProperty("sep") ? callbackParams.sep : "/";

                                    itemres[i] = ((dateStr, sep="-") => {
                                        dArr = dateStr.split(sep);  // ex input "2010-01-18"
                                        return dArr[2]+ "/" +dArr[1]+ "/" +dArr[0]; //ex out: "18/01/10"
                                    })(itemres[i], sep);
                                break;

                                case 'getLinkFullPath':
                                    itemres[i] = __utils__.getElementByXPath(`//*[@href='${itemres[i]}']`).href;
                                break;

                                case 'convertToDateObject':

                                    let resetdaytime = callbackParams.hasOwnProperty("resetdaytime") ? callbackParams.resetdaytime : false;

                                    itemres[i] = new Date(itemres[i]);
                                    itemres[i].setHours(new Date().getHours());
                                break;

                                case 'parseDateToJSTimestamp':
                                    itemres[i] = Date.parse(itemres[i]);
                                break;

                                case 'parseJSON':
                                    //console.log(` [utils:ParseComplexData] parseJSON: ${itemres[i]}`);
                                    try {
                                        itemres[i] = JSON.parse(itemres[i]);
                                    }
                                    catch (e) {
                                        console.log(" [utils:ParseComplexData] Malformed JSON, skipping step...");
                                    }
                                break;

                                case "mapextract":

                                    let mapobject = {};

                                    for(let field in callbackParams) {

                                        if(callbackParams.hasOwnProperty(field)) {
                                            mapobject[field] = __utils__.extract(itemres[i], callbackParams[field]);
                                        }
                                    }

                                    itemres[i] = mapobject;

                                    break;

                                case "unixToJSTimestamp":
                                    itemres[i] = itemres[i] * 1000;
                                break;

                                case "timeconverter":

                                    let timeConverter = UNIX_timestamp => {
                                        var a = new Date(UNIX_timestamp * 1000);
                                        var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
                                        var year = a.getFullYear();
                                        //var month = months[a.getMonth()];
                                        var month = a.getMonth()+1; month = (month>9?"":"0") + month;
                                        var date = a.getDate(); date = (date>9?"":"0") + date;
                                        var hour = a.getHours(); hour = (hour>9?"":"0") + hour;
                                        var min = a.getMinutes(); min = (min>9?"":"0") + min;
                                        var sec = a.getSeconds(); sec = (sec>9?"":"0") + sec;
                                        var time = year + '-' + month + '-'+ date + 'T' + hour + ':' + min + ':' + sec ;
                                        return time;
                                    };

                                    itemres[i] = timeConverter(itemres[i]);

                                    break;

                                case "tolowercase":
                                    if(typeof itemres[i] === 'string' || itemres[i] instanceof String) {
                                        itemres[i] = itemres[i].toLowerCase();
                                    }
                                break;

                                case "tostring":
                                    itemres[i] = itemres[i].toString();
                                break;

                            } // END SWITCH

                            if(typeof itemres[i] === 'string' || itemres[i] instanceof String) {
                                // Eventually, remove any trailing space char
                                itemres[i] = itemres[i].trim();
                            }

                        } // END FOR EACH items matching the XPath criteria

                        for (let j = tmpvalues.length - 1; j >= 0; j--) {
                            itemres.push(tmpvalues[j]);
                        }

                    } // End deals with the callbacks pipe, applying callbacks one by one, sequentially, in order they appear

                    // Remove empty items
                    for (let k = itemres.length - 1; k >= 0; k--) {

                        if (itemres[k] === "" || itemres[k]===null)
                            itemres.splice(k, 1);
                    }

                    /**
                     * Assign final metadata value to corresponding label
                     * - If label ends with 's' or '*', get the full array values
                     * - If label does not end with 's' or '*', only retrieve the first value
                     */
                    if(Array.isArray(itemres) && itemres.length)
                        datares[label.replace(/\*$/i, "")] = /[s|\*]$/i.test(label) ? itemres : itemres[0];
                }
            }
        }
    }
    else {

        if(/^_.+_$/i.test(mapping)) {
        
            var fieldlabel = mapping.match(/^_(.+)_$/i);

            switch(fieldlabel[1]) {

                case "url":
                    datares = [document.URL];
                    break;

                /*
                case "userid":
                    if(this.user.hasOwnProperty("id")) {
                        datares = [this.user.id];
                    }
                    break;
                */

                case "date":
                    datares = [__utils__.dateHandler.getCurrentDateTime("-")];
                    break;

                /*
                case "ip":
                    if(this.user.hasOwnProperty("ip")) {
                        datares = [this.user.ip];
                    }
                    break;

                case "query":
                    if(this.user.hasOwnProperty("query")) {
                        datares = [this.user.query];
                    }
                    break;
                */
                default:
                    datares = fieldlabel[1];
            }
        }
        else {

            let matchres = mapping.match(/(.+)\/@([a-z-_0-9]+)$/i);
            
            if (matchres!==null && __utils__.exists(matchres[1])) {

                if(matchres[2]==="html" || matchres[2]==="xml")
                    datares = __utils__.getSelectorHTML(matchres[1], true);
                else
                    datares = __utils__.getElementsAttributes(matchres[1], matchres[2]);
            }
            else
                datares = __utils__.getElemTexts(mapping);
        }
    }
    //console.log(" [parseComplexData] Returning this data: "+ JSON.stringify(datares, null,2));
    return datares;
};



var helper = helper || class {

    /**
     * @param {!Object} obj
     * @return {boolean}
     */
    static isString(obj) {
        return typeof obj === 'string' || obj instanceof String;
    }

        /**
     * @param {!Object} obj
     * @return {boolean}
     */
    static isNumber(obj) {
        return typeof obj === 'number' || obj instanceof Number;
    }
};


    //const {helper} = Helper;

var _Utils_ = _Utils_ || class {

 /**
   * @param {string} selector
   * @param {!Object=} options
   * @return {!Promise}
   */
  static waitForSelector(selector, options = {}) {

    const timeout = options.timeout || 30000;
    const waitForVisible = !!options.visible;
    const waitForHidden = !!options.hidden;
    const polling = waitForVisible || waitForHidden ? 'raf' : 'mutation';


    /**
     * @param {string} selector
     * @param {boolean} waitForVisible
     * @return {boolean}
     */
    let predicate = function _predicate(selector=selector, waitForVisible=waitForVisible, waitForHidden=waitForHidden) {

      const node = document.evaluate(selector, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;

      /*
      if (!node) return false;
      if (!waitForVisible) return true;

      //const style = window.getComputedStyle(node);

      try {
        var style = window.getComputedStyle(node, null);
      } catch (e) {
        return false;
      }

      return style && style.display !== 'none' && style.visibility !== 'hidden';
      */

      if (!node)
        return waitForHidden;

      if (!waitForVisible && !waitForHidden)
        return true;

      const style = window.getComputedStyle(node);
      
      const isVisible = style && style.visibility !== 'hidden' && hasVisibleBoundingBox();
      return (waitForVisible === isVisible || waitForHidden === !isVisible);

      /**
       * @return {boolean}
       */
      function hasVisibleBoundingBox() {
        const rect = node.getBoundingClientRect();
        return !!(rect.top || rect.bottom || rect.width || rect.height);
      }
    }

    return this.waitForFunction(predicate, {timeout, polling}, selector, waitForVisible, waitForHidden);
  }


  static waitWhileVisible(selector, options = {}) {
    
        const timeout = options.timeout || 30000;
        const waitUntilHidden = !!options.visible;
        const polling = waitUntilHidden ? 'raf' : 'mutation';
    
        /**
         * @param {string} selector
         * @param {boolean} waitForVisible
         * @return {boolean}
         */
        let predicate = function _predicate(selector=selector, waitUntilHidden=waitUntilHidden) {
    
          const node = document.evaluate(selector, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
    
          if (!node) return true;
    
          if (!waitUntilHidden)  return false;
    
          //const style = window.getComputedStyle(node);
    
          try {
            var style = window.getComputedStyle(node, null);
          } catch (e) {
            return false;
          }
    
          return !!(style && style.display !== 'none' && style.visibility !== 'hidden');
        }

        return this.waitForFunction(predicate, {timeout, polling}, selector, waitUntilHidden);
      }


  /**
   * @param {function()} pageFunction
   * @param {!Object=} options
   * @return {!Promise}
   */
    static waitForFunction(predicateFunc, options = {}, ...args) {
    
        const timeout = options.timeout || 30000;
        const polling = options.polling || 'raf';

        return new WaitTask(predicateFunc, polling, timeout, ...args).promise;
    }
};



var WaitTask = WaitTask || class {

    /**
     * @param {string} predicateBody
     * @param {string} polling
     * @param {number} timeout
     */
    constructor(predicateBody, polling, timeout, ...args) {

        if (helper.isString(polling))
            console.assert(polling === 'raf' || polling === 'mutation', 'Unknown polling option: ' + polling);
        else if (helper.isNumber(polling))
            console.assert(polling > 0, 'Cannot poll with non-positive interval: ' + polling);
        else
            throw new Error('Unknown polling options: ' + polling);


        this._polling = polling;
        this._timeout = timeout;
        this._predicateArgs = args;

        /**
         * @param {string} predicateBody
         * @param {string} polling
         * @param {number} timeout
         * @return {!Promise<boolean>}
         */
        var waitForPredicatePageFunction = async function (predicate=predicateBody, polling=this._polling, timeout=this._timeout, args = this._predicateArgs) {
            
            let timedOut = false;
            setTimeout(() => timedOut = true, timeout);
                
            if (polling === 'raf')
                await pollRaf();
            else if (polling === 'mutation')
                await pollMutation();
            else if (typeof polling === 'number')
                await pollInterval(polling);
            return !timedOut;
                
            /**
             * @return {!Promise<!Element>}
             */
            function pollMutation() {

                if (predicate(...args))
                    return Promise.resolve();
            
                let fulfill;
                const result = new Promise(x => fulfill = x);
                const observer = new MutationObserver(mutations => {

                if (timedOut || predicate(...args)) {
                    observer.disconnect();
                    fulfill();
                }
                });

                observer.observe(document, {
                    childList: true,
                    subtree: true,
                    attributes: true
                });
                return result;
            }
                
            /**
             * @return {!Promise}
             */
            function pollRaf() {
                
                let fulfill;
                const result = new Promise(x => fulfill = x);
                
                onRaf();
                return result;
            
                function onRaf() {
                if (timedOut || predicate(...args))
                    fulfill();
                else
                    requestAnimationFrame(onRaf);
                }
            }
                
            /**
             * @param {number} pollInterval
             * @return {!Promise}
             */
            function pollInterval(pollInterval) {

                let fulfill;
                const result = new Promise(x => fulfill = x);

                onTimeout();
                return result;
            
                function onTimeout() {
                    if (timedOut || predicate(...args))
                        fulfill();
                    else
                        setTimeout(onTimeout, pollInterval);
                }
            }
        };


        this._pageScript = waitForPredicatePageFunction;
        this._runCount = 0;
        this.promise = new Promise((resolve, reject) => {
            this._resolve = resolve;
            this._reject = reject;
        });
        
        // Since page navigation requires us to re-install the pageScript, we should track
        // timeout on our end.
        this._timeoutTimer = setTimeout(() => this.terminate(new Error(`waiting failed: timeout ${timeout}ms exceeded`)), timeout);
        this.rerun();
    }
   
    /**
     * @param {!Error} error
     */
    terminate(error) {
        this._terminated = true;
        this._reject(error);
        this._cleanup();
    }
    
    async rerun() {

        const runCount = ++this._runCount;
        let success = false;
        let error = null;

        try {
            //success = await eval(this._pageScript);
            success = await this._pageScript();
        } catch (e) {
            error = e;
        }
    
        if (this._terminated || runCount !== this._runCount)
            return;
    
        // Ignore timeouts in pageScript - we track timeouts ourselves.
        if (!success && !error)
            return;
    
        // When the page is navigated, the promise is rejected.
        // We will try again in the new execution context.
        if (error && error.message.includes('Execution context was destroyed'))
            return;
    
        // We could have tried to evaluate in a context which was already
        // destroyed.
        if (error && error.message.includes('Cannot find context with specified id'))
            return;
    
        if (error)
            this._reject(error);
        else
            this._resolve();
    
        this._cleanup();
    }
    
    _cleanup() {
        clearTimeout(this._timeoutTimer);
        this._runningTask = null;
    }
}

window._Utils_ = _Utils_;


window.__utils__ = {

    /**
     * Convert an ArrayBuffer to an UTF-8 String
     * @param {ArrayBuffer} buffer 
    */
    arrayBufferToString (buffer) {

        const bufView = new Uint8Array(buffer);
        const length = bufView.length;

        let result = '';
        let addition = Math.pow(2,8)-1;

        for(var i = 0;i<length;i+=addition){

            if(i + addition > length){
                addition = length - i;
            }
            result += String.fromCharCode.apply(null, bufView.subarray(i,i+addition));
        }

        return result;
    },
    
    /**
     * 
     */
    dateHandler : {
              
        getDate(separator, minusday) {
        
            separator = (separator===null || separator===undefined || separator==="")? "/" : "-";
            minusday = (minusday===null || minusday===undefined || minusday==="" || minusday !== parseInt(minusday, 10))? null : minusday;
        
            const today = new Date();
        
            if(minusday!==null)
                today.setDate(today.getDate()-minusday);
        
            let dd = today.getDate();
            let mm = today.getMonth()+1; //January is 0!
            const yyyy = today.getFullYear();
        
            if(dd<10)
                dd='0'+dd
        
            if(mm<10)
                mm='0'+mm
        
            return yyyy +separator+ mm +separator+ dd;
        },
            
        getCurrentTime() {
            let currentime = new Date().toLocaleTimeString().match(/(\d+:\d+:\d+)/i)[1];
            return currentime;
        },
        
        getCurrentDateTime(separator) {
            return this.getDate(separator) + " " + this.getCurrentTime();
        }
    },
    
    count(selector) {
        try {
            return this.getElementsByXPath(selector).length;
        } catch (e) {
            return 0;
        }
    },

    /**
     * Decodes a base64 encoded string. Succeeds where window.atob() fails.
     *
     * @param  String  str  The base64 encoded contents
     * @return string
     */
    decode(str) {
        /*eslint max-statements:0, complexity:0 */
        var c1, c2, c3, c4, i = 0, len = str.length, out = "";
        var BASE64_DECODE_CHARS = [
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63,
            52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1,
            -1,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
            15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1,
            -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
            41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1
        ];

        while (i < len) {
            do {
                c1 = BASE64_DECODE_CHARS[str.charCodeAt(i++) & 0xff];
            } while (i < len && c1 === -1);
            if (c1 === -1) {
                break;
            }
            do {
                c2 = BASE64_DECODE_CHARS[str.charCodeAt(i++) & 0xff];
            } while (i < len && c2 === -1);
            if (c2 === -1) {
                break;
            }
            out += String.fromCharCode(c1 << 2 | (c2 & 0x30) >> 4);
            do {
                c3 = str.charCodeAt(i++) & 0xff;
                if (c3 === 61) {
                    return out;
                }
                c3 = BASE64_DECODE_CHARS[c3];
            } while (i < len && c3 === -1);
            if (c3 === -1) {
                break;
            }
            out += String.fromCharCode((c2 & 0XF) << 4 | (c3 & 0x3C) >> 2);
            do {
                c4 = str.charCodeAt(i++) & 0xff;
                if (c4 === 61) {
                    return out;
                }
                c4 = BASE64_DECODE_CHARS[c4];
            } while (i < len && c4 === -1);
            if (c4 === -1) {
                break;
            }
            out += String.fromCharCode((c3 & 0x03) << 6 | c4);
        }
        return out;
    },

    extract(data, regexparam) {

        let regex = null;
        let datares = null;
        let isNum = false;

        // Make sure we don't try to extract String subpart from null-string
        if(!data) return null;

        if(typeof regexparam === 'string' || regexparam instanceof String) {

            if(/\(\\d\+\)/.test(regexparam)) {
                isNum=true;
            }

            regex = new RegExp(regexparam);
        }

        if (regex !== null && (regex instanceof RegExp || (regex === Object(regex) && regex.constructor.name==="RegExp"))) {

            //console.log("Extracting data from: " + data);

            //if(data!==null)
            rgxres = data.match(regex);


            if(rgxres !== null && rgxres!==undefined) {

                // IF dealing with a 'multiple cases' regex extraction (contains '|' ; i.e boolean 'OR' operator)
                // Find the first non-null, non-undefined match
                if(/\|/.test(regex)) {
                    let [, ...matches] = rgxres;
                    [datares] = matches.filter( item => item!==undefined&&item!==null);
                }
                else
                    datares = rgxres[1];

                if(isNum) {
                    datares = parseInt(rgxres[1]);
                }

                //console.log("Regex res: " + JSON.stringify(rgxres));
            }
            else datares = "";
        }
        else {
            datares = data;
        }

        return datares;
    },

    /**
     * @see: https://stackoverflow.com/questions/19098797/fastest-way-to-flatten-un-flatten-nested-json-objects
     * @see: https://github.com/hughsk/flat/blob/master/index.js
     * @param {Object} obj 
     * @param {Array} prefix 
     * @param {Object} current 
     */
    flatten (obj, prefix, current) {

        prefix = prefix || []
        current = current || {}

        // Remember kids, null is also an object!
        if (typeof (obj) === 'object' && obj !== null) {
            Object.keys(obj).forEach(key => {
                this.flatten(obj[key], prefix.concat(key), current);
            });
        } else {
            current[prefix.join('.')] = obj;
        }

        return current;
    },

    /**
     * Alias for the "getElementByXPath" method
     * Retrieves a single DOM element matching a given XPath expression.
     *
     * @param  String            selector  The XPath expression
     * @param  HTMLElement|null  scope       Element to search child elements within
     * @return HTMLElement or null
     */
    getElem(selector, scope) {
        return this.getElementByXPath(selector, scope);
    },


    /**
     * Given an XPath selector, retrieve all matching nodes text contents
     * @param {String} selector 
     * @return {Array}
     */
    getElemText(selector) {

        let text = "", element = this.getElementByXPath(selector);
        
        if (element) {

            // Replace 'br' nodes with "\n" because node.textContent does delete the line break involved by 'br'
            if(element.innerHTML) 
                element.innerHTML = element.innerHTML.replace(/<br>/gi, '\n');
    
            let elemcontent =  element.textContent || element.innerText;

            if(elemcontent!=="" && elemcontent!==null && elemcontent !== undefined) {
                // Remove any trailing space/comma character, replace all multiple spaces by one (keep LF+CR)
                let cleanedText = elemcontent.replace(/^\s+|^,|[^\S\r\n]+$/gm,'').replace(/[^\S\r\n]+/g, ' ');
                text = cleanedText.trim();
            }
        }
    
        return text;
    },


    /**
     * Given an XPath selector, retrieve all matching nodes text contents
     * @param {String} selector 
     * @return {Array}
     */
    getElemTexts(selector) {

        let textEls = [], elements = this.getElementsByXPath(selector);
        
        if (elements && elements.length) {

            // Replace 'br' nodes with "\n" because node.textContent does delete the line break involved by 'br'
            elements.map(node => {if(node.innerHTML) node.innerHTML = node.innerHTML.replace(/<br>/gi, '\n');});
    
            Array.prototype.forEach.call(elements, function _forEach(element) {
                let elemcontent =  element.textContent || element.innerText;

                if(elemcontent!=="" && elemcontent!==null && elemcontent !== undefined) {
                    // Remove any trailing space/comma character, replace all multiple spaces by one (keep LF+CR)
                    let cleanedText = elemcontent.replace(/^\s+|^,|[^\S\r\n]+$/gm,'').replace(/[^\S\r\n]+/g, ' ');
                    textEls.push(cleanedText.trim());
                }
            });
        }
    
        return textEls;
    },

    /**
     * Retrieves the value of an attribute on the first element matching the provided DOM XPath selector
     * @param {String} selector A DOM XPath selector
     * @param {String} attribute The attribute name to lookup
     */
    getElemAttr(selector, attribute) {
        return this.getElementByXPath(selector).getAttribute(attribute);
    },

    getElementsAttributes(selector, attribute) {

        //console.log(` [getElementsAttributes] Retrieving attribute '${attribute}' from node: ${selector}`);

        let attributes = [], elements = this.getElementsByXPath(selector);

        if (elements && elements.length) {

            Array.prototype.forEach.call(elements, element => {                  
                attributes.push(element.getAttribute(attribute));
            });
        }

        return attributes;
    },


    /**
     * Retrieves current HTML code matching the provided XPath selector.
     * Returns the HTML contents for the whole page if no arg is passed.
     *
     * @param  String   selector  A DOM XPath selector
     * @param  Boolean  outer     Whether to fetch outer HTML contents (default: false)
     * @return String
     */
    getSelectorHTML(selector, outer) {
        let element = this.getElementByXPath(selector);
        return outer ? element.outerHTML : element.innerHTML;
    },


    hasVisibleBoundingBox(elem) {
        const rect = elem.getBoundingClientRect();
        return !!(rect.top || rect.bottom || rect.width || rect.height);
    },

    /**
     * Checks if a given DOM element is visible in remote page.
     *
     * @param  Object   element  DOM element
     * @return Boolean
     */
    elementVisible(elem) {

        let style;

        try {
            style = window.getComputedStyle(elem, null);
        } catch (e) {
            return false;
        }

        if(style.visibility === 'hidden' || style.display === 'none') return false;
        //let cr = elem.getBoundingClientRect();
        //return cr.width > 0 && cr.height > 0;

        //return this.hasVisibleBoundingBox(elem);

        const rect = elem.getBoundingClientRect();
        return !!(rect.top || rect.bottom || rect.width || rect.height);
    },


    /**
     * Base64 encodes a string, even binary ones. Succeeds where
     * window.btoa() fails.
     *
     * @param  String  str  The string content to encode
     * @return string
     */
    encode(str) {
        /*eslint max-statements:0 */
        var out = "", i = 0, len = str.length, c1, c2, c3;
        var BASE64_ENCODE_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        while (i < len) {
            c1 = str.charCodeAt(i++) & 0xff;
            if (i === len) {
                out += BASE64_ENCODE_CHARS.charAt(c1 >> 2);
                out += BASE64_ENCODE_CHARS.charAt((c1 & 0x3) << 4);
                out += "==";
                break;
            }
            c2 = str.charCodeAt(i++);
            if (i === len) {
                out += BASE64_ENCODE_CHARS.charAt(c1 >> 2);
                out += BASE64_ENCODE_CHARS.charAt((c1 & 0x3) << 4 | (c2 & 0xF0) >> 4);
                out += BASE64_ENCODE_CHARS.charAt((c2 & 0xF) << 2);
                out += "=";
                break;
            }
            c3 = str.charCodeAt(i++);
            out += BASE64_ENCODE_CHARS.charAt(c1 >> 2);
            out += BASE64_ENCODE_CHARS.charAt((c1 & 0x3) << 4 | (c2 & 0xF0) >> 4);
            out += BASE64_ENCODE_CHARS.charAt((c2 & 0xF) << 2 | (c3 & 0xC0) >> 6);
            out += BASE64_ENCODE_CHARS.charAt(c3 & 0x3F);
        }
        return out;
    },


    /**
     * Checks if a given DOM element exists in remote page.
     *
     * @param  String  selector  XPath selector
     * @return Boolean
     */
    exists(selector) {
        try {
            return this.getElementsByXPath(selector).length > 0;
        } catch (e) {
            return false;
        }
    },


    /**
     * Fetches innerText within the element(s) matching a given XPath selector
     * selector.
     *
     * @param  String  selector  A XPath selector
     * @return String
     */
    fetchText(selector) {
        
        let text = '', elements = this.getElementsByXPath(selector);
        if (elements && elements.length) {
            Array.prototype.forEach.call(elements, function _forEach(element) {
                text += element.textContent || element.innerText || element.value || '';
            });
        }
        return text;
    },

    form_urlencoded(str) {
        return encodeURIComponent(str)
        .replace(/%20/g, '+')
        .replace(/[!'()*]/g, (c) => {
            return '%' + c.charCodeAt(0).toString(16);
        });
    },


    /**
     * Downloads a resource behind an url and returns its base64-encoded
     * contents.
     *
     * @param  String  url     The resource url
     * @param  String  method  The request method, optional (default: GET)
     * @param  Object  data    The request data, optional
     * @return String          Base64 contents string
     */
    getBase64(url, method, data) {
        return this.encode(this.getBinary(url, method, data));
    },

    /**
     * Retrieves string contents from a binary file behind an url. Silently
     * fails but log errors.
     *
     * @param   String   url     Url.
     * @param   String   method  HTTP method.
     * @param   Object   data    Request parameters.
     * @return  String
     */
    getBinary(url, method, data) {
        try {
            return this.sendAJAX(url, method, data, false, {
                overrideMimeType: "text/plain; charset=x-user-defined"
            });
        } catch (e) {
            if (e.name === "NETWORK_ERR" && e.code === 101) {
                console.log("getBinary(): Unfortunately, casperjs cannot make cross domain ajax requests");
            }
            console.log("getBinary(): Error while fetching " + url + ": " + e);
            return "";
        }
    },
    

    /**
     * Retrieves total document height.
     * http://james.padolsey.com/javascript/get-document-height-cross-browser/
     *
     * @return {Number}
     */
    getDocumentHeight() {
        return Math.max(
        Math.max(document.body.scrollHeight, document.documentElement.scrollHeight),
        Math.max(document.body.offsetHeight, document.documentElement.offsetHeight),
        Math.max(document.body.clientHeight, document.documentElement.clientHeight)
        );
    },

    /**
     * Retrieves total document width.
     * http://james.padolsey.com/javascript/get-document-width-cross-browser/
     *
     * @return {Number}
     */
    getDocumentWidth() {
        return Math.max(
            Math.max(document.body.scrollWidth, document.documentElement.scrollWidth),
            Math.max(document.body.offsetWidth, document.documentElement.offsetWidth),
            Math.max(document.body.clientWidth, document.documentElement.clientWidth)
        );
    },


    /**
     * Retrieves bounding rect coordinates of the HTML element matching the
     * provided XPath selector in the following form:
     *
     * {top: y, left: x, width: w, height:, h}
     *
     * @param  String  selector
     * @return Object or null
     */
    getElementBounds(selector) {

        try {
            var clipRect = this.getElementByXPath(selector).getBoundingClientRect();
            return {
                top: clipRect.top,
                left: clipRect.left,
                width: clipRect.width,
                height: clipRect.height
            };
        } catch (e) {
            console.log("Unable to fetch bounds for element " + selector, "warning");
        }
    },


    /**
     * Retrieves the list of bounding rect coordinates for all the HTML elements matching the
     * provided XPath selector, in the following form:
     *
     * [{top: y, left: x, width: w, height:, h},
     *  {top: y, left: x, width: w, height:, h},
     *  ...]
     *
     * @param  String  selector
     * @return Array
     */
    getElementsBounds(selector) {

        var elements = this.getElementsByXPath(selector);

        try {
            return Array.prototype.map.call(elements, (element) => {

                const clipRect = element.getBoundingClientRect();

                return {
                    top: clipRect.top,
                    left: clipRect.left,
                    width: clipRect.width,
                    height: clipRect.height
                };
            });
        } catch (e) {
            console.log("Unable to fetch bounds for elements matching " + selector, "warning");
        }
    },


    /**
     * Retrieves information about the node matching the provided selector.
     *
     * @param  String|Object  selector  XPath selector
     * @return Object
     */
    getElementInfo(selector) {

        const element = this.getElementByXPath(selector);
        const bounds = this.getElementBounds(selector);
        let attributes = {};

        [].forEach.call(element.attributes, (attr) => {
            attributes[attr.name.toLowerCase()] = attr.value;
        });

        return {
            nodeName: element.nodeName.toLowerCase(),
            attributes: attributes,
            tag: element.outerHTML,
            html: element.innerHTML,
            text: element.textContent || element.innerText,
            x: bounds.left,
            y: bounds.top,
            width: bounds.width,
            height: bounds.height,
            visible: this.visible(selector)
        };
    },

    /**
     * Retrieves information about the nodes matching the provided selector.
     *
     * @param  String|Object  selector  XPath selector
     * @return Array
     */
    getElementsInfo(selector) {

        var bounds = this.getElementsBounds(selector);
        var eleVisible = this.elementVisible;

        return [].map.call(this.getElementsByXPath(selector), (element, index) => {

            var attributes = {};

            [].forEach.call(element.attributes, (attr) => {
                attributes[attr.name.toLowerCase()] = attr.value;
            });
            
            return {
                nodeName: element.nodeName.toLowerCase(),
                attributes: attributes,
                tag: element.outerHTML,
                html: element.innerHTML,
                text: element.textContent || element.innerText,
                x: bounds[index].left,
                y: bounds[index].top,
                width: bounds[index].width,
                height: bounds[index].height,
                visible: eleVisible(element)
            };
        });
    },

    /**
     * Retrieves a single DOM element matching a given XPath expression.
     *
     * @param  String            expression  The XPath expression
     * @param  HTMLElement|null  scope       Element to search child elements within
     * @return HTMLElement or null
     */
    getElementByXPath(expression, scope) {
        scope = scope || document;
        let a = document.evaluate(expression, scope, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
        if (a.snapshotLength > 0) {
            return a.snapshotItem(0);
        }
    },

    /**
     * Retrieves all DOM elements matching a given XPath expression.
     *
     * @param  String            expression  The XPath expression
     * @param  HTMLElement|null  scope       Element to search child elements within
     * @return Array
     */
    getElementsByXPath(expression, scope) {
        scope = scope || document;
        let nodes = [];
        let a = document.evaluate(expression, scope, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
        for (let i = 0; i < a.snapshotLength; i++) {
            nodes.push(a.snapshotItem(i));
        }
        return nodes;
    },

    mouseOver(selector) {
        let event = new MouseEvent('mouseover', { 'view': window, 'bubbles': true, 'cancelable': true });
        this.getElementByXPath(selector).dispatchEvent(event);
    },

    /**
    * Removes all DOM elements matching a given XPath expression.
    *
    * @param  {String}  expression  The XPath expression
    * @return Array
    */
    removeElementsByXPath(expression) {

        let elements = this.getElementsByXPath(expression);
  
        if (elements && elements.length) {
            console.log(` [UTILS.removeElementsByXPath] Removing these nodes: ${expression}`);
            Array.prototype.forEach.call(elements, (element) => {
                element.parentNode.removeChild(element);
                //element.remove();
            });
        }
    },

    /**
     * Remove attributes from elements matching the XPath query
     * @param {String} expression 
     * @param {String} attribute 
     */
    removeElementsAttribute(expression, attribute) {
        
        let items = this.getElementsByXPath(expression);

        // Set elements' new attribute and assign given value
        Array.prototype.map.call(items, function(e, rank) {
            e.removeAttribute(attribute);
        });
    },

    /**
     * Remove attribute from element matching the XPath query
     * @param {String} expression 
     * @param {String} attribute 
     */
    removeElementAttribute(expression, attribute) {

        let item = this.getElementByXPath(expression);

        if(item)
            item.removeAttribute(attribute);
    },


    scrollElementIntoView(expression, alignWithTop=true) {

        const handle = this.getElementByXPath(expression);
        
        if(handle)
            handle.scrollIntoView(alignWithTop);
    },

    /**
     * Scrolls current document to x, y coordinates.
     *
     * @param  {Number} x X position
     * @param  {Number} y Y position
     */
    scrollTo(x,y) {
        window.scrollTo(parseInt(x || 0, 10), parseInt(y || 0, 10));
    },

    /**
     * Scrolls current document up to its bottom.
     */
    scrollToBottom() {
        this.scrollTo(0, this.getDocumentHeight());
    },


    selectOption (selectXPath, value) {

        let optionXPath = selectXPath+"/option[@value='"+ value +"' or .='"+ value +"']";

        // Get specified select option element
        let optionElem = this.getElementByXPath(optionXPath);
        optionElem.selected = true; // Change specified select option to selected

        // Create 'change' html event
        let eventObject_change = document.createEvent('HTMLEvents');
        eventObject_change.initEvent('change', true, true, window, 1);

        // Create 'click' mouse event
        let eventObject_click = document.createEvent('MouseEvents');
        eventObject_click.initMouseEvent('click', true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
        
        // Get specified select elemeent and fire two events on it: change + mouse click
        let selectElem = this.getElementByXPath(selectXPath);
        selectElem.dispatchEvent(eventObject_change);
        selectElem.dispatchEvent(eventObject_click);
    },


    selectorExists(xpath) {

        let count = this.count(xpath);

        return new Promise((resolve, reject) => {
            if(count)
                resolve(count);
            else
                reject(count);
        });
    },

    /**
     * Performs an AJAX request.
     *
     * @param   String   url      Url.
     * @param   String   method   HTTP method (default: GET).
     * @param   Object   data     Request parameters.
     * @param   Boolean  _async    Asynchroneous request? (default: false)
     * @param   Object   settings Other settings when perform the ajax request like some undocumented
     * Request Headers.
     * WARNING: an invalid header here may make the request fail silently.
     * @return  String            Response text.
     */
    sendAJAX(url, method, data, _async, settings) {

        var xhr = new XMLHttpRequest(),
        dataString = "",
        dataList = [];

        var CONTENT_TYPE_HEADER = "Content-Type";

        method = method && method.toUpperCase() || "GET";

        var contentTypeValue = settings && settings.contentType || "application/x-www-form-urlencoded";

        xhr.open(method, url, !!_async);

        console.log("sendAJAX(): Using HTTP method: '" + method + "'", "debug");

        if (settings && settings.overrideMimeType) {
            xhr.overrideMimeType(settings.overrideMimeType);
        }

        if (settings && settings.headers) {
            for (var header in settings.headers) {
                if (header === CONTENT_TYPE_HEADER) {
                    // this way Content-Type is correctly overriden,
                    // otherwise it is concatenated by xhr.setRequestHeader()
                    // see https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/setRequestHeader
                    // If the header was already set, the value will be augmented.
                    contentTypeValue = settings.headers[header];
                } else {
                    xhr.setRequestHeader(header, settings.headers[header]);
                }
            }
        }
        if (method === "POST") {
            if (typeof data === "object") {
                for (var k in data) {
                    if (data.hasOwnProperty(k)) {
                        dataList.push(this.form_urlencoded(k) + "=" +
                        this.form_urlencoded(data[k].toString()));
                    }
                }
                dataString = dataList.join('&');
                console.log("sendAJAX(): Using request data: '" + dataString + "'", "debug");
            } else if (typeof data === "string") {
                dataString = data;
            }
            xhr.setRequestHeader(CONTENT_TYPE_HEADER, contentTypeValue);
        }
        xhr.send(method === "POST" ? dataString : null);
        return xhr.responseText;
    },


    /**
     * Set a specific HTML node element's attribute value
     * @param {String} expression 
     * @param {String} attribute 
     * @param {String} value 
     */
    setElementAttribute(expression, attribute, value) {
        
        let item = this.getElementByXPath(expression);

        if(item)
            item.setAttribute(attribute, value); 
    },


    /**
     * Set specific HTML nodes elements' attribute value
     * @param {String} expression 
     * @param {String} attribute 
     * @param {String} value 
     */
    setElementsAttribute(expression, attribute, value) {
        
        let items = this.getElementsByXPath(expression);

		// Set elements' new attribute and assign given value
		Array.prototype.map.call(items, function(e, rank) {
			e.setAttribute(attribute, value); 
		});
    },

    /**
     * Checks if any element matching a given selector is visible in remote page.
     *
     * @param  String  selector  XPath selector
     * @return Boolean
     */
    visible(selector) {
        return [].some.call(this.getElementsByXPath(selector), this.elementVisible);
    },

    /**
     * Checks if all elements matching a given selector are visible in remote page.
     *
     * @param  String  selector  XPath selector
     * @return Boolean
     */
    allVisible(selector) {
        return [].every.call(this.getElementsByXPath(selector), this.elementVisible);
    }
};