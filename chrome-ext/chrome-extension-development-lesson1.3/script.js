// Page Scripts
let mapping = {
    nextref: "//*[@class='modal-header']//button[@title='Suivant']/@data-id-offre",
    prevref: "//*[@class='modal-header']//button[@title='Précédent']/@data-id-offre",
    pagenum: ["//div[@class='modal-header']//span[@class='item-courant']", {extract: "(\\d+)"}],
    totalcount: ["//div[@class='modal-header']//span[@class='nb-total']", {extract: "(\\d+)"}],
    data: {
        title: "//div[@id='detailOffreVolet']/h2",
        location: [
            "//div[@id='detailOffreVolet']/h2/following-sibling::p[1]/text()",
            {remove: /-$/}
        ],
        date: [
            "//div[@id='detailOffreVolet']/h2/following-sibling::p[contains(text(), 'Actualisé le') or contains(text(), 'Publié le')]/text()",
            {extract: /(?:Actualisé|Publié)\sle\s(.+\s\d{4})/}
        ],
        reference: [
            "//div[@id='detailOffreVolet']/h2/following-sibling::p[contains(text(), 'Actualisé le') or contains(text(), 'Publié le')]/text()",
            {extract: /offre n°\s(.+)/}
        ],
        salary: [
            "//div[@id='detailOffreVolet']//dd[contains(text(), 'Salaire')]", 
            {extract: /Salaire\s:(.+)/}
        ],
        contact: "//dl/dt[span/@title='Contact']/following-sibling::dd[1]",
        email: "//dl/dt[span/@class='icon-mail']/following-sibling::dd[1]/a",
        phone: "//dl/dt[span/@class='icon-phone']/following-sibling::dd[1]/a",
    }
};

let errorUnavailable = "//h2[@id='labelPopinExceptionReport']";
let nextActiveOffer = "//div[@id='zoneAfficherListeOffres']//li[contains(@class, 'result') and not(contains(@class, 'active'))][preceding-sibling::li[contains(@class, 'active')] or parent::ul/preceding-sibling::ul/li[contains(@class, 'active')]][1]/@data-id-offre";
let _mapping = {
    nextActiveOffer: nextActiveOffer
};

function doPromiseIfExists(xpath, treatmentCb, validationMsg) {

    return __utils__.selectorExists(xpath).then(count => {

        return new Promise( (resolve, reject) => {
            setTimeout( () => {
                treatmentCb().then(()=>{
                    console.log(validationMsg || "Button less has been found");
                    resolve();
                }).catch(errmsg=>{
                    reject(errmsg);
                });
                
            }, Math.floor((Math.random()+0.25) * 500)*2);
        });
        
    }, count => {
        console.log("HTML node could not be found: " + xpath);
        throw("NODE_NOT_FOUND");
    });
}


function timerPromise() {
    console.log(" [timerPromise] Entering time Promise...");
    return new Promise( (resolve, reject) => {
        setTimeout( () => {
            resolve();
        }, Math.floor((Math.random()+0.25) * 500)*2);
    });
}


chrome.extension.onMessage.addListener(function (message, sender, sendResponse) {

    switch (message.type) {

        case "init-step":
            console.log("Initialization step...Location is: " + message.location);
            //console.dir(__utils__.getElemTexts("//a"));
            console.log("Message: " + JSON.stringify(message, null, 2));
            
            //__utils__.getElementByXPath("//input[@value='CDI']").click();
            //__utils__.selectOption("//select[@id='idemission']", "3");


            doPromiseIfExists("//a[@id='pagelink_0']", () => {

                sessionStorage.pagenum = 1;

                chrome.storage.local.clear(function() {
                    var error = chrome.runtime.lastError;
                    if (error) {
                        console.error(error);
                    }
                    else console.log("Local storage has been successfully cleared !");
                });
                

                return timerPromise().then(() => {
                    console.log("[ In-Promise] About to click on first result link...");
                    __utils__.getElementByXPath("//a[@id='pagelink_0']").click();
                });
            }, "First result link has been found, and clicked ...")
            .catch(errmsg => {
                if (errmsg==="NODE_NOT_FOUND") {
                    console.log(" First result link could not be found ! ");
                    alert("First result link could not be found !");
                }
                throw(errmsg);
            });

            break;

        case "url-changed":

            let visibleXPath = "//div[@class='modal-content'][.//div[@id='detailOffreVolet']/h2][.//span[@class='item-courant'][.='"+sessionStorage.pagenum+"']]";

            if(/detail\/[0-9A-Z]+$/.test(message.changeInfo.url)) {

                console.log(" [onMessage] Got 'url-changed' notification, with current pagenum: " + sessionStorage.pagenum);
                console.log(" [onMessage] Changes: " + JSON.stringify(message.changeInfo, null, 2));
                console.log(` [onMessage] Check if item is visible: ${visibleXPath}`);

                _Utils_.waitForSelector(visibleXPath).then(() => {
                
                    let datares = parseComplexData(mapping);
                    let tmpdata = parseComplexData(_mapping);

                    sessionStorage.nextActiveOffer = tmpdata.nextActiveOffer;
                    console.log(` [onMessage] Next active valid offer: ${tmpdata.nextActiveOffer}`);

                    chrome.storage.local.get({
                        dataset:[]//put defaultvalues if any
                    }, data => {

                        let dataset = data.dataset;
                        //console.log("Storage data contents: " + JSON.stringify(data, null, 2 ));
                        //update(data.dataset); //storing the storage value in a variable and passing to update function

                        dataset.push(datares.data);
                        //then call the set to update with modified value
                        chrome.storage.local.set({
                            dataset: dataset
                        }, function() {
                            console.log("Updated dataset with new result");
                        });
                    });

                    return datares;
                }, (err) => {
                    console.log(` [onMessage] Could not find item: ${visibleXPath}`);
                    throw 'INVALID_PAGE_NUMBER';
                })
                .then( datares => {

                    //sessionStorage._datares.push(datares.data);
                    console.log("Datares: " + JSON.stringify(datares, null, 2));
                    
                    timerPromise().then(() => {

                        if(datares.pagenum == datares.totalcount) {

                            console.log(` [onMessage] url-changed - Current page number reached total count: ${datares.pagenum}`);
                            console.log(` [onMessage] url-changed - Preparing CSV file...`);

                            chrome.storage.local.get("dataset", data => {

                                //console.log("Storage data contents: " + JSON.stringify(data, null, 2 ));

                                var csv = Papa.unparse({
                                    fields: Object.keys(mapping.data),
                                    data: data.dataset
                                }, {
                                    quotes: true,
                                    quoteChar: '"',
                                });
    
                                console.log("CSV string: " + csv);
    
                                let blob = new Blob([csv], {type: 'text/csv'}); 
                                let link = window.document.createElement("a"); 
    
                                let date = new Date();
                                link.href = window.URL.createObjectURL(blob); 
                                link.download = "pole-emploi-export_"+date.toLocaleDateString("fr-fr").replace(/\//g,'-')+".csv"; 
    
                                document.body.appendChild(link); 
                                link.click(); 
                                document.body.removeChild(link);
                                sendResponse({alldone: true});
                            });
                        }
                        else {
                            console.log(` [onMessage] url-changed: Sending response back to Background page...`);
                            sendResponse(datares);
                        }
                            
                    });
                })
                .catch((err) => {

                    if(err === 'INVALID_PAGE_NUMBER') {

                        console.log(` [onMessage] INVALID_PAGE_NUMBER: ${visibleXPath}`);

                        let datares = parseComplexData(mapping);
                        let tmpdata = parseComplexData(_mapping);

                        sessionStorage.nextActiveOffer = tmpdata.nextActiveOffer;
                        sessionStorage.pagenum = datares.pagenum+1;
                        datares.nextref = tmpdata.nextActiveOffer;

                        // Voluntarily change the next page button offer reference with next valid available offer
                        __utils__.setElementAttribute("//*[@class='modal-header']//button[@title='Suivant']", "data-id-offre", tmpdata.nextActiveOffer);

                        console.log(` [onMessage] Next active valid offer: ${tmpdata.nextActiveOffer}`);

                        sendResponse(datares);
                    }
                    else throw err;
                });
            }
            
            break;

        case "all-done":
            alert("I'm done ! : )");
            break;

        case "click-next-page":

            if(message.hasOwnProperty("nextref") || message.hasOwnProperty("prevref")) {

                let nextpageXPath = message.hasOwnProperty("nextref") ?
                "//*[@class='modal-header']//button[@title='Suivant'][@data-id-offre='"+message.nextref+"']":
                "//*[@class='modal-header']//button[@title='Précédent'][@data-id-offre='"+message.prevref+"']/following-sibling::button[@title='Suivant']";

                doPromiseIfExists(nextpageXPath, () => {

                    sessionStorage.pagenum++;

                    return timerPromise().then(() => {
                        console.log(" [onMessage] click-next-page - About to click on next page: " + nextpageXPath);
                        __utils__.getElementByXPath(nextpageXPath).click();
                    });

                }, "Next page button has been found, and clicked: " + nextpageXPath)
                .catch(err => {
                    console.log(" [onMessage] click-next-page - Error has been caught : " + err);
                    if (err==="NODE_NOT_FOUND") {
                        console.log(" [onMessage] click-next-page - Next page button could not be found ! ");
                        alert(" Next page button could not be found !");
                    }
                    throw(err);
                });
            }

            break;
        
    }
    return true;
});

/*
window.__utils__ = {
    
    dateHandler : {
              
        getDate: function(separator, minusday) {
        
            separator = (separator===null || separator===undefined || separator==="")? "/" : "-";
            minusday = (minusday===null || minusday===undefined || minusday==="" || minusday !== parseInt(minusday, 10))? null : minusday;
        
            var today = new Date();
        
            if(minusday!==null)
                today.setDate(today.getDate()-minusday);
        
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();
        
            if(dd<10)
                dd='0'+dd
        
            if(mm<10)
                mm='0'+mm
        
            return yyyy +separator+ mm +separator+ dd;
        },
            
        getCurrentTime: function() {
            var currentime = new Date().toLocaleTimeString().match(/(\d+:\d+:\d+)/i)[1];
            return currentime;
        },
        
        getCurrentDateTime: function(separator) {
            return this.getDate(separator) + " " + this.getCurrentTime();
        }
    },
    
    count(selector) {
        try {
            return this.getElementsByXPath(selector).length;
        } catch (e) {
            return 0;
        }
    },

    
    decode(str) {
        //eslint max-statements:0, complexity:0 
        var c1, c2, c3, c4, i = 0, len = str.length, out = "";
        var BASE64_DECODE_CHARS = [
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63,
            52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1,
            -1,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
            15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1,
            -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
            41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1
        ];

        while (i < len) {
            do {
                c1 = BASE64_DECODE_CHARS[str.charCodeAt(i++) & 0xff];
            } while (i < len && c1 === -1);
            if (c1 === -1) {
                break;
            }
            do {
                c2 = BASE64_DECODE_CHARS[str.charCodeAt(i++) & 0xff];
            } while (i < len && c2 === -1);
            if (c2 === -1) {
                break;
            }
            out += String.fromCharCode(c1 << 2 | (c2 & 0x30) >> 4);
            do {
                c3 = str.charCodeAt(i++) & 0xff;
                if (c3 === 61) {
                    return out;
                }
                c3 = BASE64_DECODE_CHARS[c3];
            } while (i < len && c3 === -1);
            if (c3 === -1) {
                break;
            }
            out += String.fromCharCode((c2 & 0XF) << 4 | (c3 & 0x3C) >> 2);
            do {
                c4 = str.charCodeAt(i++) & 0xff;
                if (c4 === 61) {
                    return out;
                }
                c4 = BASE64_DECODE_CHARS[c4];
            } while (i < len && c4 === -1);
            if (c4 === -1) {
                break;
            }
            out += String.fromCharCode((c3 & 0x03) << 6 | c4);
        }
        return out;
    },


    
    getElem(selector, scope) {
        return this.getElementByXPath(selector, scope);
    },

    getElemTexts(_xpath) {

        var textEls = [], elements = this.getElementsByXPath(_xpath);
        
        if (elements && elements.length) {
    
            Array.prototype.forEach.call(elements, function _forEach(element) {
                elemcontent =  element.textContent || element.innerText;
                textEls.push(elemcontent.replace(/^\s+|\s+$|\n/gm,'').replace(/\s+/g, ' '));
            });
        }
    
        return textEls;
    },

 
    getElemAttr(selector, attribute) {
        return this.getElementByXPath(selector).getAttribute(attribute);
    },

    getElementsAttributes(selector, attribute) {

        var attributes = [], elements = this.getElementsByXPath(selector);

        if (elements && elements.length) {

            Array.prototype.forEach.call(elements, element => {                  
                attributes.push(element.getAttribute(attribute));
            });
        }

        return attributes;
    },


 
    getSelectorHTML(selector, outer) {
        var element = this.getElementByXPath(selector);
        return outer ? element.outerHTML : element.innerHTML;
    },

    elementVisible(elem) {

        var style;

        try {
            style = window.getComputedStyle(elem, null);
        } catch (e) {
            return false;
        }
        if(style.visibility === 'hidden' || style.display === 'none') return false;
        var cr = elem.getBoundingClientRect();
        return cr.width > 0 && cr.height > 0;
    },

    
    encode(str) {
        // eslint max-statements:0 
        var out = "", i = 0, len = str.length, c1, c2, c3;
        var BASE64_ENCODE_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        while (i < len) {
            c1 = str.charCodeAt(i++) & 0xff;
            if (i === len) {
                out += BASE64_ENCODE_CHARS.charAt(c1 >> 2);
                out += BASE64_ENCODE_CHARS.charAt((c1 & 0x3) << 4);
                out += "==";
                break;
            }
            c2 = str.charCodeAt(i++);
            if (i === len) {
                out += BASE64_ENCODE_CHARS.charAt(c1 >> 2);
                out += BASE64_ENCODE_CHARS.charAt((c1 & 0x3) << 4 | (c2 & 0xF0) >> 4);
                out += BASE64_ENCODE_CHARS.charAt((c2 & 0xF) << 2);
                out += "=";
                break;
            }
            c3 = str.charCodeAt(i++);
            out += BASE64_ENCODE_CHARS.charAt(c1 >> 2);
            out += BASE64_ENCODE_CHARS.charAt((c1 & 0x3) << 4 | (c2 & 0xF0) >> 4);
            out += BASE64_ENCODE_CHARS.charAt((c2 & 0xF) << 2 | (c3 & 0xC0) >> 6);
            out += BASE64_ENCODE_CHARS.charAt(c3 & 0x3F);
        }
        return out;
    },



    exists(selector) {
        try {
            return this.getElementsByXPath(selector).length > 0;
        } catch (e) {
            return false;
        }
    },


   
    fetchText(selector) {
        
        var text = '', elements = this.getElementsByXPath(selector);
        if (elements && elements.length) {
            Array.prototype.forEach.call(elements, function _forEach(element) {
                text += element.textContent || element.innerText || element.value || '';
            });
        }
        return text;
    },

    form_urlencoded(str) {
        return encodeURIComponent(str)
        .replace(/%20/g, '+')
        .replace(/[!'()*]/g, function(c) {
            return '%' + c.charCodeAt(0).toString(16);
        });
    },


   
    getBase64(url, method, data) {
        return this.encode(this.getBinary(url, method, data));
    },

    
    getBinary(url, method, data) {
        try {
            return this.sendAJAX(url, method, data, false, {
                overrideMimeType: "text/plain; charset=x-user-defined"
            });
        } catch (e) {
            if (e.name === "NETWORK_ERR" && e.code === 101) {
                console.log("getBinary(): Unfortunately, casperjs cannot make cross domain ajax requests");
            }
            console.log("getBinary(): Error while fetching " + url + ": " + e);
            return "";
        }
    },
    

    getDocumentHeight() {
        return Math.max(
        Math.max(document.body.scrollHeight, document.documentElement.scrollHeight),
        Math.max(document.body.offsetHeight, document.documentElement.offsetHeight),
        Math.max(document.body.clientHeight, document.documentElement.clientHeight)
        );
    },

    
    getDocumentWidth() {
        return Math.max(
            Math.max(document.body.scrollWidth, document.documentElement.scrollWidth),
            Math.max(document.body.offsetWidth, document.documentElement.offsetWidth),
            Math.max(document.body.clientWidth, document.documentElement.clientWidth)
        );
    },


    
    getElementBounds(selector) {

        try {
            var clipRect = this.getElementByXPath(selector).getBoundingClientRect();
            return {
                top: clipRect.top,
                left: clipRect.left,
                width: clipRect.width,
                height: clipRect.height
            };
        } catch (e) {
            console.log("Unable to fetch bounds for element " + selector, "warning");
        }
    },


  
    getElementsBounds(selector) {
        var elements = this.getElementsByXPath(selector);
        try {
            return Array.prototype.map.call(elements, function(element) {
                var clipRect = element.getBoundingClientRect();
                return {
                    top: clipRect.top,
                    left: clipRect.left,
                    width: clipRect.width,
                    height: clipRect.height
                };
            });
        } catch (e) {
            console.log("Unable to fetch bounds for elements matching " + selector, "warning");
        }
    },


   
    getElementInfo(selector) {

        var element = this.getElementByXPath(selector);
        var bounds = this.getElementBounds(selector);
        var attributes = {};

        [].forEach.call(element.attributes, function(attr) {
            attributes[attr.name.toLowerCase()] = attr.value;
        });

        return {
            nodeName: element.nodeName.toLowerCase(),
            attributes: attributes,
            tag: element.outerHTML,
            html: element.innerHTML,
            text: element.textContent || element.innerText,
            x: bounds.left,
            y: bounds.top,
            width: bounds.width,
            height: bounds.height,
            visible: this.visible(selector)
        };
    },

    getElementsInfo(selector) {

        var bounds = this.getElementsBounds(selector);
        var eleVisible = this.elementVisible;

        return [].map.call(this.getElementsByXPath(selector), function(element, index) {

            var attributes = {};

            [].forEach.call(element.attributes, function(attr) {
                attributes[attr.name.toLowerCase()] = attr.value;
            });
            return {
                nodeName: element.nodeName.toLowerCase(),
                attributes: attributes,
                tag: element.outerHTML,
                html: element.innerHTML,
                text: element.textContent || element.innerText,
                x: bounds[index].left,
                y: bounds[index].top,
                width: bounds[index].width,
                height: bounds[index].height,
                visible: eleVisible(element)
            };
        });
    },

    getElementByXPath(expression, scope) {
        scope = scope || document;
        var a = document.evaluate(expression, scope, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
        if (a.snapshotLength > 0) {
            return a.snapshotItem(0);
        }
    },

    getElementsByXPath(expression, scope) {
        scope = scope || document;
        var nodes = [];
        var a = document.evaluate(expression, scope, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
        for (var i = 0; i < a.snapshotLength; i++) {
            nodes.push(a.snapshotItem(i));
        }
        return nodes;
    },

 
    removeElementsByXPath(expression) {
        var a = document.evaluate(expression, document, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);
        for (var i = 0; i < a.snapshotLength; i++) {
            a.snapshotItem(i).parentNode.removeChild(a.snapshotItem(i));
        }
    },

    scrollTo(x,y) {
        window.scrollTo(parseInt(x || 0, 10), parseInt(y || 0, 10));
    },

 
    scrollToBottom() {
        this.scrollTo(0, this.getDocumentHeight());
    },


    selectOption (selectXPath, value) {

        var optionXPath = selectXPath+"/option[@value='"+ value +"' or .='"+ value +"']";

        // Get specified select option element
        var optionElem = this.getElementByXPath(optionXPath);
        optionElem.selected = true; // Change specified select option to selected

        // Create 'change' html event
        var eventObject_change = document.createEvent('HTMLEvents');
        eventObject_change.initEvent('change', true, true, window, 1);

        // Create 'click' mouse event
        var eventObject_click = document.createEvent('MouseEvents');
        eventObject_click.initMouseEvent('click', true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
        
        // Get specified select elemeent and fire two events on it: change + mouse click
        var selectElem = this.getElementByXPath(selectXPath);
        selectElem.dispatchEvent(eventObject_change);
        selectElem.dispatchEvent(eventObject_click);
    },

    selectorExists(xpath) {

        let count = this.count(xpath);

        return new Promise((resolve, reject) => {  
        if(count)
            resolve(count);
        else
            reject(count);
        });
    },


  
    setElementAttribute(expression, attribute, value) {
        
        let item = this.getElementByXPath(expression);

        if(item)
            item.setAttribute(attribute, value); 
    },

   
    sendAJAX(url, method, data, _async, settings) {

        var xhr = new XMLHttpRequest(),
        dataString = "",
        dataList = [];

        var CONTENT_TYPE_HEADER = "Content-Type";

        method = method && method.toUpperCase() || "GET";

        var contentTypeValue = settings && settings.contentType || "application/x-www-form-urlencoded";

        xhr.open(method, url, !!_async);

        console.log("sendAJAX(): Using HTTP method: '" + method + "'", "debug");

        if (settings && settings.overrideMimeType) {
            xhr.overrideMimeType(settings.overrideMimeType);
        }

        if (settings && settings.headers) {
            for (var header in settings.headers) {
                if (header === CONTENT_TYPE_HEADER) {
                    // this way Content-Type is correctly overriden,
                    // otherwise it is concatenated by xhr.setRequestHeader()
                    // see https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/setRequestHeader
                    // If the header was already set, the value will be augmented.
                    contentTypeValue = settings.headers[header];
                } else {
                    xhr.setRequestHeader(header, settings.headers[header]);
                }
            }
        }
        if (method === "POST") {
            if (typeof data === "object") {
                for (var k in data) {
                    if (data.hasOwnProperty(k)) {
                        dataList.push(this.form_urlencoded(k) + "=" +
                        this.form_urlencoded(data[k].toString()));
                    }
                }
                dataString = dataList.join('&');
                console.log("sendAJAX(): Using request data: '" + dataString + "'", "debug");
            } else if (typeof data === "string") {
                dataString = data;
            }
            xhr.setRequestHeader(CONTENT_TYPE_HEADER, contentTypeValue);
        }
        xhr.send(method === "POST" ? dataString : null);
        return xhr.responseText;
    },

 
    visible(selector) {
        return [].some.call(this.getElementsByXPath(selector), this.elementVisible);
    },


    allVisible(selector) {
        return [].every.call(this.getElementsByXPath(selector), this.elementVisible);
    }
};



var objcopy = function _objcopy(o) {

    var out, v, key;
    out = Array.isArray(o) ? [] : {};

    for (key in o) {
    
        v = o[key];
    
        if(v instanceof RegExp || (v === Object(v) && v.constructor.name==="RegExp")) {
            out[key] = v;
        }
        else {
            out[key] = (typeof v === "object") ? objcopy(v) : v;
        }
    }
    return out;
};
        

function parseComplexData(mapping, wrapperXPath) {

    //console.log(" [parseComplexData] Entering parsing data step...");

    wrapperXPath = (wrapperXPath===undefined || wrapperXPath===null)? "" : wrapperXPath;
        
    var datares = {};

    // IF mapping is an object
    if(mapping === Object(mapping) && !Array.isArray(mapping)) {

        //console.log("Mapping object: " + JSON.stringify(mapping, null, 2));

        var wrapperitemscount = 0;

        if (mapping.hasOwnProperty("_wrapper_")) {

            // Append current wrapper XPath to the context existing one, if any
            wrapperXPath+= mapping._wrapper_;

            // Copy the mapping object in order to gelete the _wrapper_ key, without affecting the mapping oject
            var _mapping = objcopy(mapping);
            
            delete _mapping._wrapper_;

            // Count the number of items wrapped
            // If current wrapper XPath contains an occurence of '[]', remove it for the items count step
            wrapperitemscount = __utils__.count(wrapperXPath.replace("[]", ""));

            console.log(" [PARSECOMPLEXDATA] wrapperitemscount : " + wrapperitemscount);

            if (wrapperitemscount) {

                datares = [];
                //var indexes = [];

                // Fill in list of indexes to use
                for (var index = 1; index <= wrapperitemscount; index++) {
                    //indexes[i] = i;

                //for(var index in indexes) {

                    // If current wrapper XPath contains an occurence of '[]', replace it with current index: [index]
                    if(/\[\]/i.test(wrapperXPath)) {
                        var _wrapperXPath = wrapperXPath.replace("[]", "["+index+"]");
                        //console.log("Wrapper XPath is now (1): " + _wrapperXPath);
                        datares.push(parseComplexData(_mapping, wrapperXPath.replace("[]", "["+index+"]")));
                    }
                    else {
                        // Else simply append the index predicat condition to the end of the wrapper XPath string
                        //datares.push(parseComplexData(_mapping, wrapperXPath + "["+index+"]"));
                        var _wrapperXPath =  "("+wrapperXPath + ")["+index+"]";
                        //console.log("Wrapper XPath is now (2): " + _wrapperXPath);
                        datares.push(parseComplexData(_mapping, "("+wrapperXPath + ")["+index+"]"));
                    }
                }
            }
        }
        else {

            var rawvalues = {};
            //console.log(" [GHOSTJS] Current context's wrapper: " + wrapperXPath);

            for(var label in mapping) {

                //console.log(" [GHOSTJS] Dealing with label= "+label+", mapping[label] = " + JSON.stringify(mapping[label]));

                if (!mapping.hasOwnProperty(label) || label==="_wrapper_") {
                    continue;
                }

                // IF dealing with subobject
                if(mapping[label] === Object(mapping[label]) && !Array.isArray(mapping[label])) {
            
                    var subobj = parseComplexData(mapping[label], wrapperXPath);

                    // IF sub object is not empty
                    if(JSON.stringify(subobj) !== JSON.stringify({})) {
                        rawvalues[label] = datares[label] = subobj;
                    }

                    // If dealing with object of type "table" 
                    // exemple: table : {_wrapper_:"//div[@id='wrapper']", label: "/span[1]", data: "/span[2]"}
                    if((label==="table" || label==="_table_") && Array.isArray(datares[label]) && datares[label].length)  {
                        
                        if(datares[label][0].hasOwnProperty("label") && datares[label][0].hasOwnProperty("data")) {

                            var _tmpdata = {};
                            Array.prototype.map.call(datares[label], (e, eindex) => {
                                _tmpdata[e.label] = e.data;
                            });

                            if(label==="_table_") {
                                //datares = _tmpdata;
                                for(var _label in _tmpdata) {
                                    datares[_label] = _tmpdata[_label];
                                }
                    
                                delete datares._table_;
                            }
                            else {
                                datares[label] = _tmpdata;
                            }
                        }
                    }
            
                }
                // IF dealing with pair: Label=>XPath
                else {

                    var callbacksPipe = {};
                    var currentXPath = mapping[label];
                    var tmpres = null;

                    // IF XPath is not a string but an array containing string + callbacks
                    if(Array.isArray(currentXPath)) {
                        callbacksPipe = currentXPath[1];
                        //console.log("Callbacks pipe: " + JSON.stringify(callbacksPipe));
                        //if(callbacksPipe.extract === {}) console.log("Extract object is empty");
                        currentXPath = currentXPath[0];
                    }

                    // IF label to copy starts or ends with ":", and do not starts with "/"
                    if (/^[^\/]/i.test(currentXPath) && (/^:/i.test(currentXPath) || /:$/i.test(currentXPath))) {

                        var copylabel = currentXPath.replace(":", "");

                        if(/^:/i.test(currentXPath)) { // IF label to copy starts with ":"

                            if(rawvalues.hasOwnProperty(copylabel)) {

                            tmpres = Array.isArray(rawvalues[copylabel])? rawvalues[copylabel].slice() : rawvalues[copylabel];
                            }
                            else {
                            console.log("Could not find property '"+copylabel+"' within the 'rawvalues' object");
                            }
                        }
                        else if(/:$/i.test(currentXPath)) { // IF label to copy ends with ":"

                            if(datares.hasOwnProperty(copylabel)) {

                                tmpres = Array.isArray(datares[copylabel])? datares[copylabel].slice() : datares[copylabel];
                            }
                            else
                                console.log("Could not find property '"+copylabel+"' within the 'datares' object");
                        }
                    }
                    else {

                        if (wrapperXPath!=="" && /\|\//i.test(currentXPath)) {
                            var xpathpartsres = currentXPath.match(/(\/[^|]+)\|(\/[^|]+)/i);
                            currentXPath = "(" + wrapperXPath + xpathpartsres[1] + "|" + wrapperXPath + xpathpartsres[2] + ")[1]";
                        }
                        else {
                            currentXPath = wrapperXPath + currentXPath;
                        }

                        tmpres = parseComplexData(currentXPath);
                    }

                    //console.log(" Got data: '" + tmpres + "', for label: '" + label + "'");

                    // Get a new fresh copy by cloning the array and getting a reference to this newly created array
                    if(tmpres!==undefined && tmpres!==null) {
                        rawvalues[label] = tmpres.slice();
                    }

                    //var itemres = tmpres;
                    var itemres = (tmpres!==undefined)? (Array.isArray(tmpres) ? tmpres : [tmpres]) : [];


                    // Deals with the callbacks pipe, applying callbacks one by one, sequentially, in order they appear
                    for(var callback in callbacksPipe) {

                        var callbackParams = callbacksPipe[callback];
                        var callbackParam = null;
                        var tmpvalues = [];

                        //console.log("Dealing with callback: '" + callback + "', for label: '" + label + "', with params: " + callbackParams);
                        //console.log("callbackParams: "+ JSON.stringify(callbackParams));

                        if(callbackParams instanceof RegExp || (callbackParams === Object(callbackParams) && callbackParams.constructor.name==="RegExp") || typeof callbackParams === 'string' || callbackParams instanceof String) {
                            callbackParam = callbackParams;
                            //console.log(" [GOOD] callbackParams detected as REGEX !");
                        }
                        else if (callbackParams === Object(callbackParams)) {
                            //console.log(" [object] callbackParams detected as OBJECT ! Properties: " + JSON.stringify(Object.getOwnPropertyNames(Object(callbackParams))));
                            //console.dir();
                            //console.log("TESTING IF REGEX : " + callbackParams.test("https://www.linkedin.com/in/raution/"));
                        }
                        else {
                            //console.log(" [warning] callbackParams type could not be detected !");
                        }

                        //console.log("Callback param: " + callbackParam.toString());
                        //console.log("Callback params: " + JSON.stringify(callbackParams));

                        // FOR EACH items found (matching the XPath criteria)
                        for (var i = 0; i < itemres.length; i++) {

                            //console.log("Dealing with callback: '" + callback + "', for label: '" + label + "', with value: " + itemres[i]);

                            switch(callback) {

                                case "atob":
                                    itemres[i] = atob(itemres[i]);
                                    break;

                                case "decodeURI":
                                    itemres[i] = decodeURIComponent(itemres[i]);
                                    break;

                                case "remove":

                                    if(typeof callbackParam === 'string' || callbackParam instanceof String) {
                                        callbackParam = new RegExp(callbackParam);
                                    }

                                    if (callbackParam !== null && (callbackParam instanceof RegExp || (callbackParam === Object(callbackParam) && callbackParam.constructor.name==="RegExp"))) {
                                        itemres[i] = itemres[i].replace(callbackParam, "");
                                    }
                                    break;

                                case "replace":

                                    if(callbackParams.hasOwnProperty("search") && callbackParams.hasOwnProperty("replace")) {
                                        itemres[i] = itemres[i].replace(callbackParams.search, callbackParams.replace);
                                    }
                                    break;

                                case "extract":

                                    var isNum = false;

                                    if(typeof callbackParam === 'string' || callbackParam instanceof String) {

                                        if(/\(\\d\+\)/.test(callbackParam)) {
                                            isNum=true;
                                        }

                                        callbackParam = new RegExp(callbackParam);
                                    }

                                    if (callbackParam !== null && (callbackParam instanceof RegExp || (callbackParam === Object(callbackParam) && callbackParam.constructor.name==="RegExp"))) {

                                        //console.log("Extracting data from: " + itemres[i]);

                                        //if(itemres[i]!==null)
                                        rgxres = itemres[i].match(callbackParam);


                                        if(rgxres !== null && rgxres!==undefined) {
                                            itemres[i] = rgxres[1];

                                            if(isNum) {
                                                itemres[i] = parseInt(rgxres[1]);
                                            }

                                            //console.log("Regex res: " + JSON.stringify(rgxres));
                                        }
                                        else itemres[i] = "";
                                    }
                                    else {
                                    //console.log("Could not detect callbackParam type...");
                                    //console.log("Callback param: " + callbackParam.toString());
                                    //console.log("Callback params: " + JSON.stringify(callbackParams));
                                    }
                                    break;

                                case "extractall":

                                    if (callbackParam !== null && (callbackParam instanceof RegExp || (callbackParam === Object(callbackParam) && callbackParam.constructor.name==="RegExp"))) {

                                        //console.log("Extracting data from: " + itemres[i]);

                                        //if(itemres[i]!==null)
                                        rgxres = itemres[i].match(callbackParam);


                                        if(rgxres !== null && rgxres!==undefined) {

                                            itemres[i] = rgxres[0];

                                            for(var j=1; j<rgxres.length; j++) {
                                                if(rgxres[j]!==null)
                                                    tmpvalues.push(rgxres[j]);
                                            }
                                        //console.log("Regex res: " + JSON.stringify(rgxres));
                                        }
                                        else itemres[i] = "";
                                    }
                                    break;

                                case "timeconverter":

                                    var timeConverter = UNIX_timestamp => {
                                        var a = new Date(UNIX_timestamp * 1000);
                                        var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
                                        var year = a.getFullYear();
                                        //var month = months[a.getMonth()];
                                        var month = a.getMonth()+1; month = (month>9?"":"0") + month;
                                        var date = a.getDate(); date = (date>9?"":"0") + date;
                                        var hour = a.getHours(); hour = (hour>9?"":"0") + hour;
                                        var min = a.getMinutes(); min = (min>9?"":"0") + min;
                                        var sec = a.getSeconds(); sec = (sec>9?"":"0") + sec;
                                        var time = year + '-' + month + '-'+ date + 'T' + hour + ':' + min + ':' + sec ;
                                        return time;
                                    };

                                    itemres[i] = timeConverter(itemres[i]);

                                    break;

                            } // END SWITCH

                            if(typeof itemres[i] === 'string' || itemres[i] instanceof String) {
                                // Eventually, remove any trailing space char
                                itemres[i] = itemres[i].trim();
                            }

                        } // END FOR EACH ITEM

                        for (var j = tmpvalues.length - 1; j >= 0; j--) {
                            itemres.push(tmpvalues[j]);
                        }
                    }

                    // Remove empty items
                    for (var k = itemres.length - 1; k >= 0; k--) {

                        if (itemres[k] === "" || itemres[k]===null)
                            itemres.splice(k, 1);
                    }

                    switch(callback) {

                        case "merge":

                            if(Array.isArray(itemres)) {
                                var separator = (typeof callbackParam === 'string' || callbackParam instanceof String) ? callbackParam : " ";
                                itemres[0] = itemres.join(separator);
                            }

                        break;
                    }

                
                    datares[label.replace(/\*$/i, "")] = /[s|\*]$/i.test(label) ? itemres : itemres[0];
                }
            }
        }
    }
    else {

        if(/^_.+_$/i.test(mapping)) {
        
            var fieldlabel = mapping.match(/^_(.+)_$/i);

            switch(fieldlabel[1]) {

                case "url":
                    datares = [document.URL];
                    break;

          

                case "date":
                    datares = [__utils__.dateHandler.getCurrentDateTime("-")];
                    break;

        
                default:
                    datares = fieldlabel[1];
            }
        }
        else {

            var matchres = mapping.match(/(.+)\/@([a-z-_0-9]+)$/i);
            
            if (matchres!==null && __utils__.exists(matchres[1])) {

                if(matchres[2]==="html" || matchres[2]==="xml")
                    datares = __utils__.getSelectorHTML(matchres[1], true);
                else
                    datares = __utils__.getElementsAttributes(matchres[1], matchres[2]);
            }
            else {
                //console.log("Getting texts from element which XPath is: " + mapping);
                datares = __utils__.getElemTexts(mapping);
                //console.log("Texts extracted: " + datares);
            }
        }
    }

    //console.log(" [parseComplexData] complete with: " + JSON.stringify(datares, null, 2));
    return datares;
}


var helper = helper || class {

    static evaluationString(fun, ...args) {
        
        return `(${fun})(${args.map(serializeArgument).join(',')})`;
    
     
        function serializeArgument(arg) {
        if (Object.is(arg, undefined))
            return 'undefined';
        return JSON.stringify(arg);
        }
    }


    static isString(obj) {
        return typeof obj === 'string' || obj instanceof String;
    }

 
    static isNumber(obj) {
        return typeof obj === 'number' || obj instanceof Number;
    }
};


    //const {helper} = Helper;

var _Utils_ = _Utils_ || class {


  static waitForSelector(selector, options = {}) {

    const timeout = options.timeout || 30000;
    const waitForVisible = !!options.visible;
    const polling = waitForVisible ? 'raf' : 'mutation';

    console.log(` [_Utils_] waitForSelector: ${selector}`);
    return this.waitForFunction(predicate, {timeout, polling}, selector, waitForVisible);


    function predicate(selector, waitForVisible) {

      //const node = document.querySelector(selector);
      const node = document.evaluate(selector, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;

      if (!node)
        return false;

      if (!waitForVisible)
        return true;

      //const style = window.getComputedStyle(node);

      try {
        var style = window.getComputedStyle(node, null);
      } catch (e) {
        return false;
      }

      return style && style.display !== 'none' && style.visibility !== 'hidden';
    }
  }


  static waitWhileVisible(selector, options = {}) {
    
        const timeout = options.timeout || 30000;
        const waitUntilHidden = !!options.visible;
        const polling = waitUntilHidden ? 'raf' : 'mutation';
    
        return this.waitForFunction(predicate, {timeout, polling}, selector, waitUntilHidden);
    
   
        function predicate(selector, waitUntilHidden) {
    
          const node = document.evaluate(selector, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
    
          if (!node)
            return true;
    
          if (!waitUntilHidden)
            return false;
    
          //const style = window.getComputedStyle(node);
    
          try {
            var style = window.getComputedStyle(node, null);
          } catch (e) {
            return false;
          }
    
          return !!(style && style.display !== 'none' && style.visibility !== 'hidden');
        }
      }

  static waitForFunction(pageFunction, options = {}, ...args) {

    const timeout = options.timeout || 30000;
    const polling = options.polling || 'raf';
    const predicateCode = 'return ' + helper.evaluationString(pageFunction, ...args);
    //console.log(" [waitForFunction] predicateCode: " + predicateCode);
    return new WaitTask(predicateCode, polling, timeout).promise;
  }
}

    //async function waitForPredicatePageFunction(predicateBody, polling, timeout) {
    window.waitForPredicatePageFunction = async function (predicateBody, polling, timeout) {
    
        const predicate = new Function(predicateBody);
        
        let timedOut = false;
        setTimeout(() => timedOut = true, timeout);
            
        if (polling === 'raf')
            await pollRaf();
        else if (polling === 'mutation')
            await pollMutation();
        else if (typeof polling === 'number')
            await pollInterval(polling);
        return !timedOut;
            
     
        function pollMutation() {

            if (predicate())
                return Promise.resolve();
        
            let fulfill;
            const result = new Promise(x => fulfill = x);
            const observer = new MutationObserver(mutations => {

            if (timedOut || predicate()) {
                observer.disconnect();
                fulfill();
            }
            });

            observer.observe(document, {
                childList: true,
                subtree: true,
                attributes: true
            });
            return result;
        }
            
      
        function pollRaf() {
            
            let fulfill;
            const result = new Promise(x => fulfill = x);
            
            onRaf();
            return result;
        
            function onRaf() {
            if (timedOut || predicate())
                fulfill();
            else
                requestAnimationFrame(onRaf);
            }
        }
            
   
        function pollInterval(pollInterval) {

            let fulfill;
            const result = new Promise(x => fulfill = x);

            onTimeout();
            return result;
        
            function onTimeout() {
            if (timedOut || predicate())
                fulfill();
            else
                setTimeout(onTimeout, pollInterval);
            }
        }
    }



var WaitTask = WaitTask || class {

 
    constructor(predicateBody, polling, timeout) {

        if (helper.isString(polling))
            console.assert(polling === 'raf' || polling === 'mutation', 'Unknown polling option: ' + polling);
        else if (helper.isNumber(polling))
            console.assert(polling > 0, 'Cannot poll with non-positive interval: ' + polling);
        else
            throw new Error('Unknown polling options: ' + polling);
    
        this._pageScript = helper.evaluationString(window.waitForPredicatePageFunction, predicateBody, polling, timeout);
        //console.log(" [WaitTask] _pageScript: " + this._pageScript);
        this._runCount = 0;
        this.promise = new Promise((resolve, reject) => {
            this._resolve = resolve;
            this._reject = reject;
        });
        // Since page navigation requires us to re-install the pageScript, we should track
        // timeout on our end.
        this._timeoutTimer = setTimeout(() => this.terminate(new Error(`waiting failed: timeout ${timeout}ms exceeded`)), timeout);
        this.rerun();
    }
    
  
    terminate(error) {
        this._terminated = true;
        this._reject(error);
        this._cleanup();
    }
    
    async rerun() {

        const runCount = ++this._runCount;
        let success = false;
        let error = null;

        try {
            success = await eval(this._pageScript);
        } catch (e) {
            error = e;
        }
    
        if (this._terminated || runCount !== this._runCount)
            return;
    
        // Ignore timeouts in pageScript - we track timeouts ourselves.
        if (!success && !error)
            return;
    
        // When the page is navigated, the promise is rejected.
        // We will try again in the new execution context.
        if (error && error.message.includes('Execution context was destroyed'))
            return;
    
        // We could have tried to evaluate in a context which was already
        // destroyed.
        if (error && error.message.includes('Cannot find context with specified id'))
            return;
    
        if (error)
            this._reject(error);
        else
            this._resolve();
    
        this._cleanup();
    }
    
    _cleanup() {
        clearTimeout(this._timeoutTimer);
        this._runningTask = null;
    }
}

window._Utils_ = _Utils_;
*/